#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include "Lista.h"

using namespace std;

void menu(int &wybor)
{
    cout<<"__________________________________________________"<<endl;
    cout<<"1. Dodaj element do listy"<<endl;
    cout<<"2. Usuń pojedyńczy element z listy"<<endl;
    cout<<"3. Usuń wszystkie elementy z listy"<<endl;
    cout<<"0. Wyjście z programu"<<endl;
    cout<<"__________________________________________________"<<endl;
    cout<<endl<<"Twój wybór: ";
	if(!(cin>>wybor))
	{
		cerr<<"Aby przejść dalej, należy wybrać numer z menu"<<endl;
	}
    cout<<endl;
}
int main()
{
    Lista<float> kotlet;

    int wybor=1;
    while(wybor!=0)
    {
        system("clear");
        kotlet.wyswietl();
        menu(wybor);

        switch(wybor)
        {
            case 0:
            break;
            case 1:
            float x;
            cout<<"Podaj liczbę, którą chcesz dodać do listy: ";
            if(!(cin>>x))
            {
                cerr<<"Aby przejść dalej, należy podać liczbę"<<endl;
            }
            kotlet.dodaj(x);
            break;
            case 2:
            cout<<"Podaj element, który chcesz usunąć z listy: ";
            if(!(cin>>x))
            {
                cerr<<"Aby przejść dalej, należy podać liczbę"<<endl;
            }
            kotlet.usun(x);
            break;
            case 3:
            cout<<"Lista zostanie usunięta!";
            cin.get();
            cin.get();
            kotlet.usun_all();
            break;
            default:
            cout<<"Proszę wybrać od 1 do 3 lub wybrać 0, aby wyjść z programu"<<endl;
            cin.get();
            cin.get();
            break;
        }
    }
	return 0;
}
