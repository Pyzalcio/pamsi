#ifndef KOLEJKA_H
#define KOLEJKA_H
#include <iostream>
#include "element.h"

using namespace std;

template <typename T>
class Kolejka
{
    private:
    element<T> *poczatek=NULL;

    public:

    void dodaj(T x);
    void wyswietl();
    void usun();
    void usun_all();
};

template <typename T>
void Kolejka<T>::dodaj(T x)
{
    element<T> *nowy = new element<T>;
    nowy->liczba=x;
    if(!poczatek)
    {
        poczatek=nowy;
    }
    else
    {
        element<T> *pomoc=poczatek;
        while(pomoc->nastepny)
        {
            pomoc=pomoc->nastepny;
        }
        pomoc->nastepny=nowy;
        nowy->nastepny=NULL;
    }
}
template <typename T>
void Kolejka<T>::usun()
{
    element<T> *pomoc=poczatek;
    if(poczatek)
    {
        cout<<"Nastąpi usunięcie pierwszego elementu kolejki!   ";
        pomoc=poczatek->nastepny;
        delete poczatek;
        poczatek=pomoc;
    }
    else
        cout<<"Kolejka jest pusta"<<endl;

}
template <typename T>
void Kolejka<T>::wyswietl()
{
    cout<<"Zawartość kolejki:"<<endl;
    if(!poczatek)
        cout<<"Kolejka jest pusta"<<endl;
    else
    {
        element<T> *pomoc=poczatek;
        while(pomoc)
        {
            cout<<pomoc->liczba<<"   ";
            pomoc=pomoc->nastepny;
        }
        cout<<endl;
    }
}
template <typename T>
void Kolejka<T>::usun_all()
{
    element<T> *pomoc=poczatek;
    while(poczatek)
    {
        pomoc=poczatek->nastepny;
        delete poczatek;
        poczatek=pomoc;
    }
}


#endif
