#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include <string>
#include <list>

using namespace std;

list <string> palList;

void perm(string testStr, int l)
{
    if(l==1)
    {
        cout<<testStr<<endl;
    }
    else
    {
        for(int i=0;i<l;i++)
        {
            swap(testStr[i],testStr[l-1]);
            perm(testStr,l-1);
            swap(testStr[i],testStr[l-1]);
        }
    }
}
bool jestPal(string testStr)
{
	int dlugosc=testStr.length();

	if( (testStr.length()==1) || (testStr.length()==0))
	{
	return true;
	}

	if( testStr[0] == testStr[dlugosc - 1])
	{
		return jestPal( testStr.substr( 1, dlugosc-2) );
    }

	else
	{
        return false;
    }
}
void dodajPal(string pal)
{
    palList.push_back(pal);
    cout<<"Łańcuch został dodany do tablicy palindromów"<<endl;
}
void usunDup()
{
    palList.sort();
    palList.unique();
    cout<<"Usunięto duplikaty z tablicy palindromów"<<endl;
    cin.get();
    cin.get();
}
void menu(int &wybor)
{
    cout<<"__________________________________________________"<<endl;
    cout<<"1. Podaj następny ciąg znaków"<<endl;
    cout<<"2. Zobacz tablicę palindromów"<<endl;
    cout<<"3. Usuń duplikaty"<<endl;
    cout<<"0. Wyjście z programu"<<endl;
    cout<<"__________________________________________________"<<endl;
    cout<<endl<<"Wybór: ";
	if(!(cin>>wybor))
	{
		cerr<<"Aby przejść dalej, należy wybrać numer z menu"<<endl;
	}
    cout<<endl;
}
void wyswietl()
{
    if (palList.empty()==1 )
    {
        cout<<"Tablica jest pusta"<<endl;
    }
    else
    {
        cout<<"Zawartość tablicy"<<endl;
        for(list<string>::iterator i=palList.begin(); i!=palList.end(); ++i)
        {
            cout<<*i<<" ";
        }
    }
    cin.get();
    cin.get();
}
void wpisywanie(string &testStr)
{
    cout<<"Proszę podać ciąg znaków: ";
    cin>>testStr;
    cout<<endl<<"Z podanego ciągu znaku można uzyskać następujące permutacje:"<<endl;
    int l=testStr.length();
    perm(testStr,l);
}
int main()
{
    string testStr;
    int wybor=1;
    while(wybor!=0)
    {
        system("clear");
        menu(wybor);

        switch(wybor)
        {
            case 1:
            wpisywanie(testStr);
            if (jestPal(testStr))
            {
                cout<<endl<<"Ciąg znaków jest palindromem"<<endl;
                dodajPal(testStr);
            }
            cin.get();
            cin.get();
            break;
            case 2:
            wyswietl();
            break;
            case 3:
            usunDup();
            break;
        }
    }

	return 0;
}
