#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include <string>
#include <list>

using namespace std;

struct Lista
{
    int liczba;
    Lista *nastepny;

    Lista()
    {
        cout<<"Podaj liczbę: ";
        cin>>liczba;
        nastepny=NULL;
    }

    void pokaz()
    {
        cout<<liczba<<" ";
    }


};
void menu(int &wybor)
{
    cout<<"__________________________________________________"<<endl;
    cout<<"1. Dodaj element do listy"<<endl;
    cout<<"2. Usuń pojedyńczy element z listy"<<endl;
    cout<<"3. Usuń wszystkie elementy z listy"<<endl;
    cout<<"0. Wyjście z programu"<<endl;
    cout<<"__________________________________________________"<<endl;
    cout<<endl<<"Twój wybór: ";
	if(!(cin>>wybor))
	{
		cerr<<"Aby przejść dalej, należy wybrać numer z menu"<<endl;
	}
    cout<<endl;
}
void wyswietl(Lista *glowa)
{
    cout<<"Zawartosc listy: "<<endl;

    while(glowa != NULL)
    {
        glowa->pokaz();
        glowa = glowa->nastepny;
    }
    cout<<endl;
}
void dodaj(Lista **glowa)
{
    Lista *nowa = new Lista;
    Lista *wsk = (*glowa), *wsk1 = NULL;
    while (wsk!=NULL)
    {
        wsk1=wsk;
        wsk=wsk->nastepny;
    }

    if ((*glowa) == NULL)
    {
    nowa->nastepny = (*glowa);
    (*glowa) = nowa;
    }

    else
    {
        wsk1->nastepny = nowa;
        nowa->nastepny = wsk;
    }
}
int usun(Lista **glowa)
{
    int x;

    if ((*glowa) == NULL)
    {
    cout<<"Lista jest pusta"<<endl;
    return 0;
    }

    cout<<"Podaj liczbę, którą chcesz usunąć z listy: ";
    cin>>x;

    Lista *wsk = (*glowa), *wsk1 = NULL;

    while(wsk != NULL && (wsk->liczba!=x))
    {
        wsk1 = wsk;
        wsk = wsk->nastepny;
    }

    if (wsk == NULL)
    {
        cout<<"Nie znaleziono podanej liczby w liście!"<<endl;
        cin.get();
        cin.get();
        return 0;
    }

    else if (wsk == (*glowa))
    {
        (*glowa) = (*glowa)->nastepny;
    }

    else
    {
        wsk1->nastepny = wsk->nastepny;
    }
    return 0;
}
int usun_all(Lista **glowa)
{
    if( (*glowa)==NULL)
    {
        cout<<"Lista jest pusta"<<endl;
        return 0;
    }

    while( (*glowa)!=NULL)
    {
        (*glowa)=(*glowa)->nastepny;
    }
    cout<<"Wszystkie elementy z listy zostaną usunięte!"<<endl;
}
int main()
{
    Lista *glowa = NULL;

    int wybor=1;
    while(wybor!=0)
    {
        system("clear");
        wyswietl(glowa);
        menu(wybor);

        switch(wybor)
        {
            case 0:
            break;
            case 1:
            dodaj(&glowa);
            break;
            case 2:
            usun(&glowa);
            break;
            case 3:
            usun_all(&glowa);
            cin.get();
            cin.get();
            break;
            default:
            cout<<"Proszę wybrać od 1 do 3 lub wybrać 0, aby wyjść z programu"<<endl;
            cin.get();
            cin.get();
            break;
        }
    }
	return 0;
}
