#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include <string>
#include <list>

using namespace std;

struct Kolejka
{
    int liczba;
    Kolejka *nastepny;
    Kolejka *poprzedni;
};
void menu(int &wybor)
{
    cout<<endl;
    cout<<"__________________________________________________"<<endl;
    cout<<"1. Dodaj element do kolejki"<<endl;
    cout<<"2. Usuń pojedyńczy element z kolejki"<<endl;
    cout<<"3. Usuń wszystkie elementy z kolejki"<<endl;
    cout<<"0. Wyjście z programu"<<endl;
    cout<<"__________________________________________________"<<endl;
    cout<<endl;
	if(!(cin>>wybor))
	{
		cerr<<"Aby przejść dalej, należy wybrać numer z menu"<<endl;
	}
    cout<<endl;
}
void wyswietl(Kolejka *glowa, Kolejka *ogon, Kolejka *wsk)
{
    if (glowa != NULL)
    {
        cout << "Zawartosc kolejki:\n";
        wsk = glowa;
        while (wsk != NULL)
        {
        cout << wsk->liczba << " ";
        wsk = wsk->nastepny;
        }
    }
    else
    {
        cout << "Kolejka jest pusta";
    }
}
void dodaj(Kolejka *&glowa, Kolejka *&ogon, Kolejka *&wsk)
{
        wsk = new Kolejka;
        cout<<"Podaj liczbę, którą chcesz dodać do kolejki: ";
        cin >> wsk->liczba;
        if (glowa == NULL)
        {
            wsk->nastepny = wsk->poprzedni = NULL;
            ogon = glowa = wsk;
        }
        else
        {
            wsk->nastepny = NULL;
            ogon->nastepny = wsk;
            wsk->poprzedni = ogon;
            ogon = wsk;
        }
}
void usun(Kolejka *&glowa, Kolejka *&ogon, Kolejka *&wsk)
{
        cout<<"Nastąpi usunięcie pierwszego elementu kolejki"<<endl;
        wsk = glowa;
        if (glowa == ogon)
        {
            glowa = ogon = NULL;
        }
        else
        {
            glowa = glowa->nastepny;
        }
        delete wsk;
        cin.get();
        cin.get();
}
void usun_all(Kolejka *&glowa, Kolejka *&ogon, Kolejka *&wsk)
{
    cout<<"Nastąpi usunięcie całej kolejki"<<endl;
    if(glowa==NULL)
    {
        cout << "Kolejka jest pusta";
    }
    else
    {
        while (glowa != NULL)
        {
        glowa = glowa->nastepny;
        }
    }
    cin.get();
    cin.get();
}
int main()
{
    Kolejka *glowa=NULL;
    Kolejka *ogon=NULL;
    Kolejka *wsk=NULL;

    int wybor=1;
    while(wybor!=0)
    {
        system("clear");
        wyswietl(glowa, ogon, wsk);
        menu(wybor);

        switch(wybor)
        {
            case 1:
            dodaj(glowa,ogon,wsk);
            break;
            case 2:
            usun(glowa,ogon,wsk);
            break;
            case 3:
            usun_all(glowa,ogon,wsk);
            break;
        }
    }
	return 0;
}
