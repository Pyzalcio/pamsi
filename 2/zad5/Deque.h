#ifndef DEQUE_H
#define DEQUE_H

#include "element.h"
#include <iostream>
#include <string>

using namespace std;

template <typename Object>
class Deque
{
private:
    element<Object> *poczatek=NULL;
    element<Object> *koniec=NULL;

public:

    /**
    * Zwraca ilość obiektów przechowywanych w deque
    */
    int rozmiar() const;

    /**
    * Zwraca true jeśli deque jest pusty
    */

    bool isEmpty() const;
    /**
    * Zwraca pierwszy obiekt w deque.
    * Wyrzuca DequeEmptyException jeśli deque jest pusty
    */

    Object& pierwszy();

    /**
    * Zwraca ostatni obiekt w deque.
    * Wyrzuca DequeEmptyException jeśli deque jest pusty
    */
    Object& ostatni();

    /**
    * Dodaje obiekt do poczatku deque’a.
    */
    //Object& wstawPierwszy(const Object& obj);
    void wstawPierwszy(const Object& x);

    /**
    * Usuwa pierwszy obiekt z deque.
    * Wyrzuca DequeEmptyException jeśli deque jest pusty
    */
    //Object& usunPierwszy() ;//throw(DequeEmptyException);
    void usunPierwszy() ;

    /**
    * Dodaje obiekt na końcu deque’a.
    */
    //Object& wstawOstatni(const Object& obj);
    void wstawOstatni(const Object& x);

    /**
    * Usuwa ostatni obiekt z deque.
    * Wyrzuca DequeEmptyException jeśli deque jest pusty
    */
    //Object& usunOstatni();// throw(DequeEmptyException);
    void usunOstatni();
    void wyswietl();
    bool palin(int r);

};

template <typename Object>
void Deque<Object>::wyswietl()
{
    cout<<"Zawartosc kolejki:"<<endl;
    if(!poczatek)
        cout<<"Kolejka jest pusta"<<endl;
    else
    {
        element<Object> *pomoc=poczatek;
        while(pomoc)
        {
            cout<<pomoc->wartosc;
            pomoc=pomoc->nastepny;
        }
        cout<<endl;
    }
}

template <typename Object>
int Deque<Object>::rozmiar() const
{
    element<Object> *pomoc=poczatek;
    int i=0;
    while(pomoc)
    {
        i++;
        pomoc=pomoc->nastepny;
    }
    return i;
}

template <typename Object>
bool Deque<Object>::isEmpty() const
{
    element<Object> *pomoc=poczatek;
    if(pomoc)
        return false;
    else
        return true;
}

template <typename Object>
Object& Deque<Object>::pierwszy()
{
    return poczatek->wartosc;
}

template <typename Object>
Object& Deque<Object>::ostatni()
{
    /*element<Object> *pomoc=poczatek;
    while(pomoc->nastepny)
        pomoc=pomoc->nastepny;
    return pomoc->wartosc;*/
    return koniec->wartosc;
}

template <typename Object>
void Deque<Object>::wstawPierwszy(const Object& x)
{
    element<Object> *nowy = new element<Object>;
    nowy->wartosc=x;
    if (poczatek)
    {
        element<Object> *pomoc = poczatek;

        poczatek=nowy;
        poczatek->nastepny=pomoc;
    }
    else
    {
        poczatek=nowy;
        koniec=nowy;
    }
}

template <typename Object>
void Deque<Object>::usunPierwszy()
{
    if( isEmpty() )
    {
        cout<<"Kolejka jest pusta"<<endl;
    }
    else
    {
        element<Object> *pomoc=poczatek;
        cout<<"Pierwszy element kolejki zostanie usunięty"<<endl;
        if(pomoc->nastepny)
        {
            pomoc=pomoc->nastepny;
        }
        else
            pomoc=NULL;

        free(poczatek);
        poczatek=pomoc;
    }
}
template <typename Object>
void Deque<Object>::wstawOstatni(const Object& x)
{
    element<Object> *nowy = new element<Object>;
    nowy->wartosc=x;

    if (poczatek)
    {

        element<Object> *pomoc=poczatek;

        while (pomoc->nastepny)
        {
            pomoc=pomoc->nastepny;
        }

        pomoc->nastepny=nowy;
        nowy->nastepny=NULL;
        koniec=nowy;
    }
    else
        wstawPierwszy(x);
}
template <typename Object>
void Deque<Object>::usunOstatni()
{
    if(isEmpty())
    {
        cout<<"Kolejka jest pusta"<<endl;
    }
    else
    {

        /*element<Object> *pomoc=poczatek;
        element<Object> *pomoc2=koniec;
        while(pomoc->nastepny)
        {
            pomoc=pomoc->nastepny;
        }
        koniec=pomoc;
        cout<<"Ostatni element kolejki zostanie usunięty"<<endl;
        if(pomoc->poprzedni)
        {
            pomoc=pomoc->poprzedni;
        }
        else
            pomoc=NULL;

        free(poczatek);
        koniec=pomoc;
        poczatek=pomoc2;*/
        element<Object> *pomoc=koniec;
        cout<<"Ostatni element kolejki zostanie usunięty"<<endl;
        if(pomoc->poprzedni)
        {
            pomoc=pomoc->poprzedni;
        }
        else
            pomoc=NULL;

        free(koniec);
        koniec=pomoc;

    }
}
template <typename Object>
bool Deque<Object>::palin(int r)
{
    element<Object> *pomoc=poczatek;
    element<Object> *pomoc2=koniec;
    //while(pomoc->wartosc==pomoc2->wartosc)
   // {
        if(r==1 || r==0)
        {
            return true;
        }
        else if(pomoc->wartosc!=pomoc2->wartosc)
            return false;
        else
        {
            pomoc=pomoc->nastepny;
            pomoc2=pomoc2->poprzedni;
            r=r-2;
            return Deque<Object>::palin(r);
        }
    //}
    //if(pomoc->wartosc!=pomoc2->wartosc)
      //  return false;
}

#endif
