#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include "Deque.h"

using namespace std;

void menu(int &wybor)
{
    cout<<"__________________________________________________"<<endl;
    cout<<"1. Dodaj element na poczatek kolejki"<<endl;
    cout<<"2. Dodaj element na koniec kolejki"<<endl;
    cout<<"3. Usuń pierwszy element kolejki"<<endl;
    cout<<"4. Usuń ostatni element kolejki"<<endl;
    cout<<"5. Pierwszy element kolejki"<<endl;
    cout<<"6. Ostatni element kolejki"<<endl;
    cout<<"7. Ilość elementów w kolejce"<<endl;
    cout<<"8. Czy jest pal"<<endl;
    cout<<"0. Wyjście z programu"<<endl;
    cout<<"__________________________________________________"<<endl;
    cout<<endl<<"Twój wybór: ";
	if(!(cin>>wybor))
	{
		cerr<<"Aby przejść dalej, należy wybrać numer z menu"<<endl;
	}
    cout<<endl;
}
/*bool jestPal(string testStr)
{
	int dlugosc=testStr.length();

	if( (testStr.length()==1) || (testStr.length()==0))
	{
	return true;
	}

	if( testStr[0] == testStr[dlugosc - 1])
	{
		return jestPal( testStr.substr( 1, dlugosc-2) );
    }

	else
	{
        return false;
    }
}*/
int main()
{
    Deque<char> kotlet;

    int wybor=1;
    while(wybor!=0)
    {
        system("clear");
        kotlet.wyswietl();
        menu(wybor);

        switch(wybor)
        {
            case 0:
            break;
            case 1:
            char x;
            cout<<"Podaj liczbę, którą chcesz dodać na początek kolejki: ";
            cin>>x;
            kotlet.wstawPierwszy(x);
            break;
            case 2:
            cout<<"Podaj liczbę, którą chcesz dodać na koniec kolejki: ";
            cin>>x;
            kotlet.wstawOstatni(x);
            break;
            case 3:
            kotlet.usunPierwszy();
            cin.get();
            cin.get();
            break;
            case 4:
            kotlet.usunOstatni();
            cin.get();
            cin.get();
            break;
            case 5:
            cout<<"Pierwszy element kolejki: "<<kotlet.pierwszy()<<endl;
            cin.get();
            cin.get();
            break;
            case 6:
            cout<<"Ostatni element kolejki: "<<kotlet.ostatni()<<endl;
            cin.get();
            cin.get();
            break;
            case 7:
            cout<<"Rozmiar kolejki: "<<kotlet.rozmiar()<<endl;;
            cin.get();
            cin.get();
            break;
            case 8:
            int r;
            r=kotlet.rozmiar();
            if(r==0)
                cout<<"Lista jest pusta"<<endl;
            if(r==1 || r==2)
                cout<<"Ciąg znaków jest palindromem"<<endl;
            else if( kotlet.palin(r) == true )
                cout<<"Ciąg znaków jest palindromem"<<endl;
            else
                cout<<"Ciąg znaków nie jest palindromem"<<endl;

            cin.get();
            cin.get();
            break;
            default:
            cout<<"Proszę wybrać od 1 do 8 lub wybrać 0, aby wyjść z programu"<<endl;
            cin.get();
            cin.get();
            break;
        }
    }
	return 0;
}
