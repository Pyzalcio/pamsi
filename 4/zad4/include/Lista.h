#ifndef LISTA_H
#define LISTA_H

#include "Element.h"
#include <iostream>
#include <string>

using namespace std;

template <typename Object>
class Lista
{
private:
    element<Object> *poczatek=NULL;
    element<Object> *koniec=NULL;

public:

    int rozmiar() const;
    bool isEmpty() const;
    Object& pierwszy();
    Object& ostatni();
    void wstawPierwszy(const Object& x);
    void usunPierwszy() ;
    void wstawOstatni(const Object& x);
    void usunOstatni();
    void wyswietl();
    void delete_all();
};

template <typename Object>
void Lista<Object>::wyswietl()
{
    cout<<"Zawartosc listy przed sortowaniem:"<<endl;
    if(!poczatek)
        cout<<"Lista jest pusta"<<endl;
    else
    {
        element<Object> *pomoc=poczatek;
        while(pomoc)
        {
            cout<<pomoc->wartosc<<", ";
            pomoc=pomoc->nastepny;
        }
        cout<<endl;
    }
}

template <typename Object>
int Lista<Object>::rozmiar() const
{
    element<Object> *pomoc=poczatek;
    int i=0;
    while(pomoc)
    {
        i++;
        pomoc=pomoc->nastepny;
    }
    return i;
}

template <typename Object>
bool Lista<Object>::isEmpty() const
{
    element<Object> *pomoc=poczatek;
    if(pomoc)
        return false;
    else
        return true;
}

template <typename Object>
Object& Lista<Object>::pierwszy()
{
    return poczatek->wartosc;
}

template <typename Object>
Object& Lista<Object>::ostatni()
{
    return koniec->wartosc;
}

template <typename Object>
void Lista<Object>::wstawPierwszy(const Object& x)
{
    element<Object> *nowy = new element<Object>;
    nowy->wartosc=x;
    if (poczatek)
    {
        element<Object> *pomoc = poczatek;

        pomoc->poprzedni=nowy;
        poczatek=nowy;
        poczatek->nastepny=pomoc;
    }
    else
    {
        poczatek=nowy;
        koniec=nowy;
    }
}

template <typename Object>
void Lista<Object>::usunPierwszy()
{
    if( isEmpty() )
    {
        cout<<"Kolejka jest pusta"<<endl;
    }
    else
    {
        element<Object> *pomoc=poczatek;
        cout<<"Pierwszy element listy zostanie usunięty"<<endl;
        if(pomoc->nastepny)
        {
            pomoc=pomoc->nastepny;
        }
        else
            pomoc=NULL;

        free(poczatek);
        poczatek=pomoc;
    }
}
template <typename Object>
void Lista<Object>::wstawOstatni(const Object& x)
{
    element<Object> *nowy = new element<Object>;
    nowy->wartosc=x;

    if (poczatek)
    {

        element<Object> *pomoc=poczatek;

        while (pomoc->nastepny)
        {
            pomoc=pomoc->nastepny;
        }

        pomoc->nastepny=nowy;
        koniec=nowy;
        koniec->poprzedni=pomoc;
    }
    else
        wstawPierwszy(x);
}
template <typename Object>
void Lista<Object>::usunOstatni()
{
    int x=rozmiar();
    if(isEmpty())
    {
        cout<<"Lista jest pusta"<<endl;
    }
    if(x==1)
    {
        delete_all();
    }
    else
    {
        element<Object> *pomoc=koniec;
        if(pomoc->poprzedni)
        {
            pomoc=pomoc->poprzedni;
        }
        else
            pomoc=NULL;
        free(koniec);
        koniec=pomoc;
        pomoc->nastepny=NULL;

    }
}
template <typename Object>
void Lista<Object>::delete_all()
{
    while(poczatek)
    {
        if( isEmpty() )
        {
            cout<<"Lista jest pusta"<<endl;
        }
        else
        {
            element<Object> *pomoc=poczatek;
            if(pomoc->nastepny)
            {
                pomoc=pomoc->nastepny;
            }
            else
                pomoc=NULL;

            free(poczatek);
            poczatek=pomoc;

        }
    }
}
#endif
