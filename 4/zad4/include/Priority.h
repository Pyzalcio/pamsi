#ifndef PRIORITY_H
#define PRIORITY_H

#include "Element.h"
#include <iostream>
#include <string>

using namespace std;

template <typename Object>
class Priority
{
private:
    element<Object> *poczatek=NULL;
    element<Object> *koniec=NULL;

public:

    int rozmiar() const;
    bool isEmpty() const;
    Object& pierwszy();
    Object& ostatni();
    void wstawPierwszy(const Object& x, const Object& k);
    void usunPierwszy();
    void delete_all();
    void wstawOstatni(const Object& x, const Object& k);
    void usunOstatni();
    void wyswietl();
};

template <typename Object>
void Priority<Object>::wyswietl()
{
    cout<<"Zawartosc listy po sortowaniu:"<<endl;
    if(!poczatek)
        cout<<"Lista jest pusta"<<endl;
    else
    {
        element<Object> *pomoc=poczatek;
        while(pomoc)
        {
            cout<<pomoc->wartosc<<", ";
            //<<"["<<pomoc->klucz<<"]"<<", ";
            pomoc=pomoc->nastepny;
        }
        cout<<endl;
    }
}

template <typename Object>
int Priority<Object>::rozmiar() const
{
    element<Object> *pomoc=poczatek;
    int i=0;
    while(pomoc)
    {
        i++;
        pomoc=pomoc->nastepny;
    }
    return i;
}

template <typename Object>
bool Priority<Object>::isEmpty() const
{
    element<Object> *pomoc=poczatek;
    if(pomoc)
        return false;
    else
        return true;
}

template <typename Object>
Object& Priority<Object>::pierwszy()
{
    return poczatek->wartosc;
}
template <typename Object>
Object& Priority<Object>::ostatni()
{
    return koniec->wartosc;
}
template <typename Object>
void Priority<Object>::wstawPierwszy(const Object& x, const Object& k)
{
	element <Object> * nowy = new element <Object>;
	nowy->wartosc=x;
	nowy->klucz=k;

	if(!poczatek)
		poczatek = koniec = nowy;

	else
	{
        element <Object> * pomoc = poczatek;
		while( (pomoc!=NULL) && (nowy->klucz >= pomoc->klucz) )
            pomoc = pomoc->nastepny;

		if (pomoc == NULL)
		{
			koniec->nastepny = nowy;
			nowy->poprzedni = koniec;
			koniec = nowy;
		}
		else if(pomoc==poczatek)
		{
			nowy->nastepny = poczatek;
			poczatek->poprzedni = nowy;
			poczatek = nowy;
		}
		else
		{
			nowy->nastepny = pomoc;
			nowy->poprzedni = pomoc->poprzedni;
			pomoc->poprzedni->nastepny = nowy;
			pomoc->poprzedni = nowy;
		}
	}
}
/*template <typename Object>
void Priority<Object>::usunPierwszy()
{
    if( isEmpty() )
    {
        cout<<"Kolejka jest pusta"<<endl;
    }
    else
    {
        element<Object> *pomoc=poczatek;
        if(pomoc->nastepny)
        {
            pomoc=pomoc->nastepny;
        }
        else
            pomoc=NULL;

        free(poczatek);
        poczatek=pomoc;
    }
}*/
template <typename Object>
void Priority<Object>::delete_all()
{
    while(poczatek)
    {
        if( isEmpty() )
        {
            cout<<"Kolejka jest pusta"<<endl;
        }
        else
        {
            element<Object> *pomoc=poczatek;
            if(pomoc->nastepny)
            {
                pomoc=pomoc->nastepny;
            }
            else
                pomoc=NULL;

            free(poczatek);
            poczatek=pomoc;
        }
    }
}
/*template <typename Object>
void Priority<Object>::wstawOstatni(const Object& x, const Object& k)
{
    element<Object> *nowy = new element<Object>;
    nowy->wartosc=x;
    nowy->klucz=k;

    if(poczatek)
    {
        element<Object> *pomoc=poczatek;

        while (pomoc->nastepny)
        {
            pomoc=pomoc->nastepny;
        }

        pomoc->nastepny=nowy;
        nowy->nastepny=NULL;
        koniec=nowy;
    }

    else
        wstawPierwszy(x, k);
}*/
template <typename Object>
void Priority<Object>::usunOstatni()
{
    if(isEmpty())
    {
        cout<<"Kolejka jest pusta"<<endl;
    }
    else if(rozmiar()==1)
    {
        delete_all();
    }
    else
    {
        element<Object> *pomoc=koniec;
        if(pomoc->poprzedni)
        {
            pomoc=pomoc->poprzedni;
        }
        else
            pomoc=NULL;
        free(koniec);
    }
}

#endif
