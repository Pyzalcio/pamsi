#include <iostream>
#include <cstdlib>
#include "Priority.h"
#include "Lista.h"

using namespace std;

void menu(int &wybor)
{
    cout<<"_________________________________________"<<endl;
    cout<<"1. Dodaj element"<<endl;
    cout<<"2. Usuń ostatni"<<endl;
    cout<<"3. Sortuj elementy"<<endl;
    cout<<"4. Usuń wszystkie elementy"<<endl;
    cout<<"0. Wyjście z programu"<<endl;
    cout<<"_________________________________________"<<endl;
    cout<<"Twój wybór: ";
    if(!(cin>>wybor))
    {
        cerr<<"Należy wybrać opcje z menu!"<<endl;
    }
}
int main()
{
    Priority<int> kotlet;
    Lista<int> pizza;
    int wybor=1;
    int si=0;
    while(wybor!=0)
    {
        system("clear");
        pizza.wyswietl();
        cout<<endl;
        kotlet.wyswietl();
        menu(wybor);
        switch(wybor)
        {
        case 1:
            int x;
            int k;
            cout<<"Dodaj: ";
            cin>>x;
            //cout<<"Klucz: ";
            //cin>>k;
            pizza.wstawOstatni(x);
            si++;
            break;
        case 2:
            cout<<"Ostatni element zostanie usunięty"<<endl;
            pizza.usunOstatni();
            cin.get();
            cin.get();
            si--;
            break;
        case 3:
            kotlet.delete_all();
            for(int i=0; i<si; i++)
            {
                x=pizza.ostatni();
                k=x;
                kotlet.wstawPierwszy(x,k);
                pizza.usunOstatni();
            }
            /*for(int i=0; i<si; i++)
            {
                x=kotlet.pierwszy();
                pizza.wstawPierwszy(x);
                kotlet.usunOstatni();
            }*/
            kotlet.wyswietl();
            cin.get();
            cin.get();
            si=0;
            break;
        case 4:
            cout<<"Lista zostanie usunięta"<<endl;
            cin.get();
            cin.get();
            pizza.delete_all();
            kotlet.delete_all();
            si=0;
        case 0:
            //cout<<"Program zostanie wyłączony!"<<endl;
            break;
        default:
            cout<<"Proszę wybrać od 1 do 4 lub wybrać 0, aby wyjść z programu"<<endl;
            cin.get();
            cin.get();
            break;
        }
    }
    return 0;
}
