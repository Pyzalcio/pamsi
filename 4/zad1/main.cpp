#include <iostream>
#include <cstdlib>
#include "priority.h"

using namespace std;

void menu(int &wybor)
{
    cout<<"_________________________________________"<<endl;
    cout<<"1. Dodaj element do kolejki prioretytowej"<<endl;
    cout<<"2. Usuń element o najmniejszym kluczu"<<endl;
    cout<<"3. Wyswietl element o najmniejszym kluczu"<<endl;
    cout<<"4. Usuń całą kolejke"<<endl;
    cout<<"0. Wyjście z programu"<<endl;
    cout<<"_________________________________________"<<endl;
    cout<<"Twój wybór: ";
    if(!(cin>>wybor))
    {
        cerr<<"Należy wybrać opcje z menu!"<<endl;
    }
}
int main()
{
    Priority<char> kotlet;
    int wybor=1;
    while(wybor!=0)
    {
        system("clear");
        kotlet.wyswietl();
        menu(wybor);
        switch(wybor)
        {
        case 1:
            char x;
            int k;
            cout<<"Dodaj: ";
            cin>>x;
            cout<<"Klucz: ";
            cin>>k;
            kotlet.wstawPierwszy(x, k);
            //kotlet.sortuj();
            break;
        case 2:
            cout<<"Element o najmniejszym kluczu zostanie usunięty ("<<kotlet.ostatni()<<")"<<endl;
            cin.get();
            cin.get();
            kotlet.usunOstatni();
            break;
        case 3:
            cout<<"Element o najmniejszym kluczu: "<<kotlet.ostatni()<<endl;
            cin.get();
            cin.get();
            break;
        case 4:
            cout<<"Kolejka zostanie usunięta"<<endl;
            cin.get();
            cin.get();
            kotlet.delete_all();
        case 0:
            //cout<<"Program zostanie wyłączony!"<<endl;
            break;
        default:
            cout<<"Proszę wybrać od 1 do 4 lub wybrać 0, aby wyjść z programu"<<endl;
            cin.get();
            cin.get();
            break;
        }
    }
    return 0;
}
