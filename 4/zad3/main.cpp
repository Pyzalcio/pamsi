#include <iostream>
#include <cstdlib>
#include "Binarne.h"
#include "Tree.h"
#define rozmiar 10
using namespace std;

void menu(int &wybor)
{
    cout<<"1. Drzewo binarne"<<endl;
    cout<<"2. Drzewo ogólne"<<endl;
    cout<<"0. Wyjście"<<endl;

    cout<<endl<<"Twój wybór: ";
    if(!(cin>>wybor))
    {
        cerr<<"Należy podać cyfre od 0 do 2"<<endl;
    }
}
int main()
{
    Binarne<int> drzewo;
    Galaz<int> *galazka=new Galaz<int>;
    Tree<int> drzewko;
    Lacznik<int> *laczenie=new Lacznik<int>;

    int wybor=1;
    while(wybor!=0)
    {
        system("clear");
        menu(wybor);
        system("clear");
        switch(wybor)
        {
        case 1:
            cout<<endl<<"           1"<<endl<<endl;
            cout<<"    2            4"<<endl<<endl;
            cout<<"3      6      5      7"<<endl<<endl;
            galazka->set_lisc(1);
            drzewo.set_root(galazka);
            drzewo.wstaw(galazka,2);
            drzewo.wstaw(galazka->get_lewe(),3);
            drzewo.wstaw(galazka,4);
            drzewo.wstaw(galazka->get_prawe(),5);
            drzewo.wstaw(galazka->get_lewe(),6);
            drzewo.wstaw(galazka->get_prawe(),7);
            drzewo.wyswietl_przej();
            /*cout<<"wyswietlenie przykladowego wezla, ktorego nastepnie usuniemy wraz z dziecmi"<<endl;
            drzewo.wyswietl_korzen(galazka->get_lewe()->get_lewe());
            drzewo.usun(galazka->get_lewe()->get_lewe());
            cout<<"po usunieciu"<<endl;
            drzewo.wyswietl_przej_pre(galazka);*/
            cout<<"Wysokość drzewa: "<<drzewo.wysokosc()<<endl;
            cin.get();
            cin.get();
            break;
        case 2:
            cout<<endl<<"        7"<<endl<<endl;
            cout<<"6  5  4  3  2  1"<<endl<<endl;
            laczenie->set_wartosc(7);
            drzewko.set_root(laczenie);
            drzewko.wstaw(laczenie,6);
            drzewko.wstaw(laczenie,5);
            drzewko.wstaw(laczenie,4);
            drzewko.wstaw(laczenie,3);
            drzewko.wstaw(laczenie,2);
            drzewko.wstaw(laczenie,1);
            drzewko.wyswietl_przej();
            cout<<"Wysokość drzewa: "<<drzewko.wysokosc()<<endl;
            cin.get();
            cin.get();
            break;
        case 0:
        break;
        default:
            cout<<"Proszę wybrać jedną opcję z menu (od 0 do 2)"<<endl;
            break;
        }
    }
    return 0;
}
