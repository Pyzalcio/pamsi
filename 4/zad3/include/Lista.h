#ifndef Lista_H
#define Lista_H

#include <string>
#include "Element.h"
#include <iostream>

using namespace std;

template<typename Object>
class Lista
{
    Element<Object> *poczatek=nullptr;
    Element<Object> *koniec=nullptr;
public:

    using ListEmptyException=string;
    Element<Object>* get_poczatek() const;
    void set_poczatek(Element<Object> *obj);
    Element<Object>* get_koniec() const;
    void set_koniec(Element<Object> *obj);
    Object& wstawPierwszy(const Object& obj);
    Object& usunPierwszy() throw(ListEmptyException);
    Object& wstawOstatni(const Object& x);
    void l_printl() throw(ListEmptyException);
    void delete_all() throw(ListEmptyException);
};

template<typename Object>
Element<Object>* Lista<Object>::get_poczatek() const
{
    return poczatek;
}

template<typename Object>
void Lista<Object>::set_poczatek(Element<Object> *x)
{
    poczatek=x;
}

template<typename Object>
Element<Object>* Lista<Object>::get_koniec() const
{
    return koniec;
}

template<typename Object>
void Lista<Object>::set_koniec(Element<Object> *x)
{
    koniec=x;
}

template<typename Object>
Object& Lista<Object>::wstawPierwszy(const Object& x)
{
    Element<Object> *p=new Element<Object>;
    p->set_wartosc(x);

    if(poczatek)
    {
        Element<Object> *pomoc=poczatek;
        poczatek=p;
        poczatek->set_nastepny(pomoc);
        pomoc->set_poprzedni(poczatek);
    }
    else
    {
        poczatek=koniec=p;
    }

    return poczatek->get_wartosc();
}

template<typename Object>
Object& Lista<Object>::usunPierwszy() throw(ListEmptyException)
{
    if(!poczatek)
    {
        string napis="Is empty";
        throw napis;
    }
    Element<Object> *pomoc=poczatek;
    if(pomoc->get_nastepny()){
        pomoc=pomoc->get_nastepny();
    }
    else
        pomoc=nullptr;
    delete poczatek;
    poczatek=pomoc;
    return poczatek->get_wartosc();
}

template<typename Object>
Object& Lista<Object>::wstawOstatni(const Object& x)
{
    Element<Object> *p=new Element<Object>;
    p->set_wartosc(x);

    if (koniec) // sprawdzamy czy to pierwszy Element listy
    {
        // w przeciwnym wypadku wędrujemy na koniec listy
        Element<Object> *pomoc=koniec;
        pomoc->set_nastepny(p);  // ostatni Element wskazuje na nasz nowy
        p->set_poprzedni(pomoc);     // ostatni nie wskazuje na nic
        koniec=p;
    }
    else
    {
        // jeżeli tak to nowy Element jest teraz początkiem listy
        poczatek=koniec=p;
    }
    return koniec->get_wartosc();
}

template<typename Object>
void Lista<Object>::l_printl() throw(ListEmptyException)
{
    if(!poczatek)
    {
        string napis="Is empty";
        throw napis;
    }
    Element<Object> *pomoc=poczatek;
    while (pomoc)
    {
        cout<<pomoc->get_wartosc()<<endl;
        pomoc=pomoc->get_nastepny();
    }
}

template<typename Object>
void Lista<Object>::delete_all() throw(ListEmptyException)
{
    if(!poczatek)
    {
        string napis="Is empty";
        throw napis;
    }
    Element<Object> *pomoc=poczatek;
    while(pomoc->get_nastepny())
    {
        poczatek=poczatek->get_nastepny();
        delete pomoc;
        pomoc=poczatek;
    }
    delete pomoc;
    poczatek=nullptr;
}

#endif
