#ifndef GALAZ_H
#define GALAZ_H

template <typename Object>
class Galaz
{
    Galaz<Object> *korzen=nullptr;
    Galaz<Object> *lewe=nullptr;
    Galaz<Object> *prawe=nullptr;
    Object lisc;
public:

    Galaz<Object>* get_korzen() const;
    void set_korzen(Galaz<Object> *x);
    Galaz<Object>* get_lewe() const;
    void set_lewe(Galaz<Object> *x);
    Galaz<Object>* get_prawe() const;
    void set_prawe(Galaz<Object> *x);
    Object& get_lisc();
    void set_lisc(const Object &x);
};


template<typename Object>
Galaz<Object>* Galaz<Object>::get_korzen() const
{
    return korzen;
}

template<typename Object>
void Galaz<Object>::set_korzen(Galaz<Object> *x)
{
    korzen=x;
}

template<typename Object>
Galaz<Object>* Galaz<Object>::get_lewe() const
{
    return lewe;
}

template<typename Object>
void Galaz<Object>::set_lewe(Galaz<Object> *x)
{
    lewe=x;
}

template<typename Object>
Galaz<Object>* Galaz<Object>::get_prawe() const
{
    return prawe;
}

template<typename Object>
void Galaz<Object>::set_prawe(Galaz<Object> *x)
{
    prawe=x;
}

template<typename Object>
Object& Galaz<Object>::get_lisc()
{
    return lisc;
}

template<typename Object>
void Galaz<Object>::set_lisc(const Object &x)
{
    lisc=x;
}


#endif
