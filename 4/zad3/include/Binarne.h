#ifndef BINARNE_H
#define BINARNE_H

#include <string>
#include <iostream>
#include <unistd.h>

#include "Galaz.h"

using namespace std;

template<typename Object>
class Binarne
{
    Galaz<Object> *root=nullptr;

public:

    using QueueEmptyException=string;

    Galaz<Object>* get_root() const;

    void set_root(Galaz<Object> *obj);
    void wyswietl_przej() throw(QueueEmptyException);
    void wyswietl_przej_in(Galaz<Object>* x);
    void wyswietl_przej_pre(Galaz<Object>* x);
    void wyswietl_przej_post(Galaz<Object>* x);

    void wyswietl_korzen(Galaz<Object>* x);

    Object wstaw(Galaz<Object>* rodzic,const Object& obj);

    Object usun(Galaz<Object>* temp);

    void delete_all(Galaz<Object>* x);

    int wysokosc(Galaz<Object> *Galaz);
    int wysokosc();
};

template<typename Object>
int Binarne<Object>::wysokosc()
{
    return wysokosc(root);
}

template<typename Object>
int Binarne<Object>::wysokosc(Galaz<Object> *Galaz)
{
    int hl=0,hp=0;
    if (Galaz->get_lewe() || Galaz->get_prawe())
    {
        if(Galaz->get_lewe())
            hl=wysokosc(Galaz->get_lewe());
        else if(Galaz->get_prawe())
            hp=wysokosc(Galaz->get_prawe());
        return( 1 +( hl > hp ? hl: hp ) );
    }
    else
        return 0;
}

template<typename Object>
Galaz<Object>* Binarne<Object>::get_root() const
{
    return root;
}

template<typename Object>
void Binarne<Object>::set_root(Galaz<Object> *obj)
{
    root=obj;
}

template<typename Object>
Object Binarne<Object>::wstaw(Galaz<Object>* rodzic,const Object& x)
{
    Galaz<Object> *p=new Galaz<Object>;
    p->set_lisc(x);

    if(!rodzic)
    {
        cerr<<"Brak gałęzi"<<endl;
    }
    else
    {
        if(rodzic->get_lewe()==nullptr)
        {
            rodzic->set_lewe(p);
            p->set_korzen(rodzic);
        }
        else if(rodzic->get_prawe()==nullptr)
        {
            rodzic->set_prawe(p);
            p->set_korzen(rodzic);
        }
        else
        {
            cerr<<"Nie może być więcej niż 2 dzieci! (prawe, lewe)"<<endl;
        }
    }
    return x;
}

template<typename Object>
void Binarne<Object>::wyswietl_przej() throw(QueueEmptyException)  //od najwiekszego
{
    if(!root)
    {
        string napis="Is empty";
        throw napis;
    }
    usleep(500000);
    cout<<endl<<"Przejście pre-order:"<<endl;
    wyswietl_przej_pre(root);
    usleep(500000);
    cout<<endl<<"Przejście in-order:"<<endl;
    wyswietl_przej_in(root);
    usleep(500000);
    cout<<endl<<"Przejście post-order:"<<endl;
    wyswietl_przej_post(root);

    cout<<endl<<endl;
}

template<typename Object>
void Binarne<Object>::wyswietl_przej_in(Galaz<Object>* x)
{
    if(x)
    {
        wyswietl_przej_in(x->get_lewe());
        cout<<x->get_lisc()<<"  ";
        wyswietl_przej_in(x->get_prawe());
    }
}

template<typename Object>
void Binarne<Object>::wyswietl_przej_pre(Galaz<Object>* x)
{
    if(x)
    {
        cout<<x->get_lisc()<<"  ";
        wyswietl_przej_pre(x->get_lewe());
        wyswietl_przej_pre(x->get_prawe());
    }
}

template<typename Object>
void Binarne<Object>::wyswietl_przej_post(Galaz<Object>* x)
{
    if(x)
    {
        wyswietl_przej_post(x->get_lewe());
        wyswietl_przej_post(x->get_prawe());
        cout<<x->get_lisc()<<"  ";
    }
}

template<typename Object>
void Binarne<Object>::wyswietl_korzen(Galaz<Object>* x)
{
    if(x)
    {
        cout<<x->get_lisc()<<endl;
    }
}

template <typename Object>
Object Binarne<Object>::usun(Galaz<Object>* temp)
{
    Object pomoc;
    if(temp->get_lewe()==nullptr && temp->get_prawe()==nullptr)
    {
        if (temp->get_korzen()->get_lewe()==temp)
            temp->get_korzen()->set_lewe(nullptr);
        else
            temp->get_korzen()->set_prawe(nullptr);
        pomoc=temp->get_lisc();
        delete temp;
    }
    else
    {
        cout<<"Nie można usunąć rodzica, kiedy ma dzieci"<<endl;
        if (temp->get_korzen()->get_lewe()==temp)
            temp->get_korzen()->set_lewe(nullptr);
        else
            temp->get_korzen()->set_prawe(nullptr);
        pomoc=temp->get_lisc();
        delete_all(temp);
    }
    return pomoc;
}

template<typename Object>
void Binarne<Object>::delete_all(Galaz<Object>* x)
{
    if(x)
    {
        delete_all(x->get_lewe());
        delete_all(x->get_prawe());
        delete x;
    }
}

#endif
