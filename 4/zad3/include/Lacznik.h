#ifndef LACZNIK_H
#define LACZNIK_H

#include "Lista.h"

template <typename Object>
class Lacznik
{
    Lacznik<Object> *korzen=nullptr;
    Lista<Lacznik*> syn;
    Object wartosc;
public:

    Lacznik<Object>* get_korzen() const;
    void set_korzen(Lacznik<Object> *x);
    Element<Lacznik<Object>*>* get_syn(const int& x) const;
    void set_syn(Lacznik<Object> *x);
    Object& get_wartosc();
    void set_wartosc(const Object &x);
    void del(const int&m);
};

template<typename Object>
void Lacznik<Object>::del(const int &x)
{
    if(x==0)
    {
        syn.usunPierwszy();
    }
    else
    {
        Element<Lacznik<Object>*> *pomoc=syn.get_poczatek();
        for(int i=0; i<x; i++)
        {
            pomoc=pomoc->get_nastepny();
        }

        Element<Lacznik<Object>*> *pomoc2=pomoc->get_nastepny();
        if(pomoc->get_nastepny()->get_nastepny()->get_wartosc()==nullptr)
        {
        cout<<pomoc->get_nastepny()->get_wartosc()<<endl;
            pomoc->set_nastepny(nullptr);
            cout<<pomoc->get_nastepny()->get_wartosc()<<endl;
        }
        else
            pomoc->set_nastepny(nullptr);
        pomoc2->set_poprzedni(pomoc);
        delete pomoc->get_nastepny();
    }
}
template<typename Object>
Lacznik<Object>* Lacznik<Object>::get_korzen() const
{
    return korzen;
}

template<typename Object>
void Lacznik<Object>::set_korzen(Lacznik<Object> *x)
{
    korzen=x;
}

template<typename Object>
Element<Lacznik<Object>*> *Lacznik<Object>::get_syn(const int& x) const
{
    Element<Lacznik<Object>*> *pomoc=syn.get_poczatek();
    for(int i=0;i<x;i++)
    {
        pomoc=pomoc->get_nastepny();
    }
    return pomoc;
}

template<typename Object>
void Lacznik<Object>::set_syn(Lacznik<Object> *x)
{
    syn.wstawOstatni(x);
}

template<typename Object>
Object& Lacznik<Object>::get_wartosc()
{
    return wartosc;
}

template<typename Object>
void Lacznik<Object>::set_wartosc(const Object &x)
{
    wartosc=x;
}

#endif
