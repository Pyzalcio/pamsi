#ifndef ELEMENT_H
#define ELEMENT_H

#include <string>
#include <iostream>

using namespace std;

template<typename Object>
class Element
{
    Element<Object> *nastepny=nullptr;
    Element<Object> *poprzedni=nullptr;
    Object wartosc;
    int klucz=0;
public:

    Element<Object>* get_nastepny() const;
    void set_nastepny(Element<Object> *x);
    Element<Object>* get_poprzedni() const;
    void set_poprzedni(Element<Object> *x);
    Object& get_wartosc();
    void set_wartosc(const Object &x);
    int& get_key();
    void set_key(const int &x);
};

template<typename Object>
Element<Object>* Element<Object>::get_nastepny() const
{
    return nastepny;
}

template<typename Object>
void Element<Object>::set_nastepny(Element<Object> *x)
{
    nastepny=x;
}

template<typename Object>
Element<Object>* Element<Object>::get_poprzedni() const
{
    return poprzedni;
}

template<typename Object>
void Element<Object>::set_poprzedni(Element<Object> *x)
{
    poprzedni=x;
}

template<typename Object>
Object& Element<Object>::get_wartosc()
{
    return wartosc;
}

template<typename Object>
void Element<Object>::set_wartosc(const Object &x)
{
    wartosc=x;
}

template<typename Object>
int& Element<Object>::get_key()
{
    return klucz;
}

template<typename Object>
void Element<Object>::set_key(const int &x)
{
    klucz=x;
}

#endif
