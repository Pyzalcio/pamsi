#ifndef KOLEJKA_H
#define KOLEJKA_H

#include <string>
#include "Element.h"
#include <iostream>

template<typename Object>
class Kolejka
{
protected:
    Element<Object> *poczatek=nullptr;
    Element<Object> *koniec=nullptr;
public:

    using QueueEmptyException=string;

    Element<Object>* get_poczatek() const;
    void set_poczatek(Element<Object> *obj);

    Element<Object>* get_koniec() const;
    void set_koniec(Element<Object> *obj);

    void l_printl() throw(QueueEmptyException);

    Object& wstawOstatni(const int& key, const Object& obj);

    Object removeMin() throw(QueueEmptyException);

    Object& Min() throw(QueueEmptyException);
};

template<typename Object>
Element<Object>* Kolejka<Object>::get_poczatek() const
{
    return poczatek;
}

template<typename Object>
void Kolejka<Object>::set_poczatek(Element<Object> *obj)
{
    poczatek=obj;
}

template<typename Object>
Element<Object>* Kolejka<Object>::get_koniec() const
{
    return koniec;
}

template<typename Object>
void Kolejka<Object>::set_koniec(Element<Object> *obj)
{
    koniec=obj;
}

template<typename Object>
Object& Kolejka<Object>::wstawOstatni(const int& klucz,const Object& wartosc)
{
    Element<Object> *p=new Element<Object>;
    p->set_wartosc(wartosc);
    p->set_key(klucz);

    if (poczatek) // sprawdzamy czy to pierwszy Element listy
    {
        // w przeciwnym wypadku wędrujemy na koniec listy TOKOM
        Element<Object> *temp=poczatek;

        if(temp->get_key()>p->get_key())
        {
            poczatek=p;
            p->set_nastepny(temp);
            temp->set_poprzedni(p);
        }
        else
        {
            while (temp->get_nastepny() && temp->get_nastepny()->get_key()<=p->get_key())
            {
                // znajdujemy wskaźnik na ostatni Element
                temp=temp->get_nastepny();
            }
            Element<Object> *pomoc=temp->get_nastepny();
            temp->set_nastepny(p);  // ostatni Element wskazuje na nasz nowy TOKOM
            p->set_nastepny(pomoc);     // ostatni nie wskazuje na nic TOKOM
            p->set_poprzedni(temp);
            if(temp==koniec)
            {
                //cout<<"koniec: "<<p->get_key()<<endl;
                koniec=p;
            }
            else
            {
                pomoc->set_poprzedni(p);
            }
        }

    }
    else
    {
        // jeżeli tak to nowy Element jest teraz początkiem listy
        poczatek=koniec=p;
    }
    return poczatek->get_wartosc();
}

template<typename Object>
void Kolejka<Object>::l_printl() throw(QueueEmptyException)
{
    if(!poczatek)
    {
        string napis="Is empty";
        throw napis;
    }
    Element<Object> *temp=poczatek;
    cout<<"klucz  wartosc"<<endl;
    while (temp)
    {
        std::cout<<temp->get_key()<<"     "<<temp->get_wartosc()<<std::endl;
        temp=temp->get_nastepny();
    }
}

template<typename Object>
Object Kolejka<Object>::removeMin() throw(QueueEmptyException)
{
    if(!poczatek)
    {
        string napis="Is empty";
        throw napis;
    }
    Element<Object> *temp=poczatek;
    Object wartosc;
    if(temp->get_nastepny()){
        temp=temp->get_nastepny();
    }
    else
        temp=nullptr;
    wartosc=poczatek->get_wartosc();
    delete poczatek;
    poczatek=temp;
    poczatek->set_poprzedni(nullptr);
    return wartosc;
}

template<typename Object>
Object& Kolejka<Object>::Min() throw(QueueEmptyException)
{
    if(!poczatek)
    {
        string napis="Is empty";
        throw napis;
    }
    return poczatek->get_wartosc();
}

#endif
