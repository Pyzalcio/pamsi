#ifndef TREE_H
#define TREE_H

#include <unistd.h>

#include "Lacznik.h"
#include "Kolejka.h"


template<typename Object>
class Tree
{
    Lacznik<Object> *root=nullptr;
public:

    using QueueEmptyException=string;

    Lacznik<Object>* get_root() const;

    void set_root(Lacznik<Object> *x);

    void wyswietl_przej() throw(QueueEmptyException);
    void wyswietl_przej_pre(Lacznik<Object>* x);
    void wyswietl_przej_post(Lacznik<Object>* x);
    void wyswietl_przej_in(Lacznik<Object>* x);
    void wyswietl_przej_pre(Element<Lacznik<Object>*>* x);
    void wyswietl_przej_post(Element<Lacznik<Object>*>* x);
    void wyswietl_przej_in(Element<Lacznik<Object>*>* x);

    void wyswietl_korzen(Lacznik<Object>* x);

    Object wstaw(Lacznik<Object>* rodzic,const Object& obj);
    Lacznik<Object>* szukajmiejsca(Lacznik<Object> *temp,const Object& rodzic);

    Object usun(Lacznik<Object>* x);

    void remove_all(Lacznik<Object>* x);
    void remove_all(Element<Lacznik<Object>*>* x);
    int wysokosc(Lacznik<Object> *x);
    int wysokosc();
};

template<typename Object>
int Tree<Object>::wysokosc()
{
    return wysokosc(root);
}

template<typename Object>
int Tree<Object>::wysokosc(Lacznik<Object> *x)
{
    Kolejka<int> h;
    if (x->get_syn(0))
    {
        int i=0;
        while(x->get_syn(i))
        {
            h.wstawOstatni(wysokosc(x->get_syn(i)->get_wartosc()),1);
            i++;
        }

        return(1+h.get_koniec()->get_key());
    }
    else
        return 0;
}

template<typename Object>
Lacznik<Object>* Tree<Object>::get_root() const
{
    return root;
}

template<typename Object>
void Tree<Object>::set_root(Lacznik<Object> *x)
{
    root=x;
}

template<typename Object>
Object Tree<Object>::wstaw(Lacznik<Object>* rodzic,const Object& x)
{
    Lacznik<Object> *p=new Lacznik<Object>;
    p->set_wartosc(x);

    if(!rodzic)
    {
        cerr<<"Brak węzła"<<endl;
    }
    else
    {
        rodzic->set_syn(p);
        p->set_korzen(rodzic);
    }
    return x;
}

template<typename Object>
void Tree<Object>::wyswietl_przej() throw(QueueEmptyException)  //od najwiekszego
{
    if(!root)
    {
        cerr<<"Pusto"<<endl;
    }
    usleep(500000);
    cout<<endl<<"Przejście pre-order:"<<endl;
    wyswietl_przej_pre(root);
    usleep(500000);
    cout<<endl<<"Przejście in-order:"<<endl;
    wyswietl_przej_in(root);
    usleep(500000);
    cout<<endl<<"Przejście post-order:"<<endl;
    wyswietl_przej_post(root);

    cout<<endl<<endl;

}

template<typename Object>
void Tree<Object>::wyswietl_przej_pre(Lacznik<Object>* x)
{
    if(x)
    {
        cout<<x->get_wartosc()<<"  ";
        int i=0;
        while(x->get_syn(i))
        {
            wyswietl_przej_pre(x->get_syn(i));
            i++;
        }
    }
}

template<typename Object>
void Tree<Object>::wyswietl_przej_pre(Element<Lacznik<Object>*>* x)
{
    cout<<x->get_wartosc()->get_wartosc()<<"  ";
    int i=0;
    while(x->get_wartosc()->get_syn(i))
    {
        wyswietl_przej_pre(x->get_wartosc()->get_syn(i));
        i++;
    }
}

template<typename Object>
void Tree<Object>::wyswietl_przej_post(Lacznik<Object>* x)
{
    if(x)
    {
        int i=0;
        while(x->get_syn(i))
        {
            wyswietl_przej_post(x->get_syn(i));
            i++;
        }
        cout<<x->get_wartosc()<<"  ";
    }
}

template<typename Object>
void Tree<Object>::wyswietl_przej_post(Element<Lacznik<Object>*>* x)
{
    int i=0;
    while(x->get_wartosc()->get_syn(i))
    {
        wyswietl_przej_post(x->get_wartosc()->get_syn(i));
        i++;
    }
    cout<<x->get_wartosc()->get_wartosc()<<"  ";
}
template<typename Object>
void Tree<Object>::wyswietl_przej_in(Lacznik<Object>* x)
{
    if(x)
    {
        int i=0;
        while(x->get_syn(i))
        {
            wyswietl_przej_post(x->get_syn(i));
            i++;
        }
        cout<<x->get_wartosc()<<"  ";
    }
}

template<typename Object>
void Tree<Object>::wyswietl_przej_in(Element<Lacznik<Object>*>* x)
{
    int i=0;
    while(x->get_wartosc()->get_son(i))
    {
        wyswietl_przej_post(x->get_wartosc()->get_son(i));
        cout<<x->get_wartosc()->get_wartosc()<<"  ";
        i++;
    }
}
template<typename Object>
void Tree<Object>::wyswietl_korzen(Lacznik<Object>* x)
{
    if(x)
    {
        cout<<x->get_wartosc()<<endl;
    }
    else
    {
        cerr<<"Brak węzła"<<endl;
    }
}

template <typename Object>
Object Tree<Object>::usun(Lacznik<Object>* x)
{
    Object pomoc;
    if(x->get_son(0)->get_wartosc()==nullptr)
    {
        int i=0;
        while(x->get_korzen()->get_son(i)->get_wartosc()==x)
            x->get_korzen()->del(i+1);

        pomoc=x->get_wartosc();
        delete x;
    }
    else
    {
        cout<<"Ojciec ma dzieci!"<<endl;
        int i=0;

        while(x->get_korzen()->get_son(i)->get_wartosc()->get_wartosc()==x->get_wartosc())
        {
            i++;
            if(x->get_korzen()->get_son(i-1)->get_wartosc()->get_wartosc()==x->get_wartosc())
                break;
        }
        x->get_korzen()->del(i-1);
        pomoc=x->get_wartosc();
        remove_all(x);
    }
    return pomoc;
}

template<typename Object>
void Tree<Object>::remove_all(Lacznik<Object>* x)
{
    if(x)
    {
        int i=0;
        while(x->get_son(i))
        {
            remove_all(x->get_son(i));;
            i++;
        }
        delete x;
    }
}
template<typename Object>
void Tree<Object>::remove_all(Element<Lacznik<Object>*>* x)
{
    int i=0;
    while(x->get_wartosc()->get_son(i))
    {
        remove_all(x->get_wartosc()->get_son(i));
        i++;
    }
    delete x->get_wartosc();
}

#endif
