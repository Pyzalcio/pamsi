#include <iostream>
#include <cstdlib>
#include <stack>
#include "Stos_tab.h"
#include "Stos.h"
#include "time.h"
#include "Lista.h"

using namespace std;

Stos_tab<int> kotlet;
stack<int> stos;
Lista<int> lista;

void startowe(int &wybor)
{
    cout<<"__________________________________________________"<<endl;
    cout<<"1. Stos bazujący na tablicy"<<endl;
    cout<<"2. Stos w oparciu o biblioteke STL"<<endl;
    cout<<"3. Test prędkości dodawania elementów do stosu"<<endl;
    cout<<"0. Wyjście z programu"<<endl;
    cout<<"__________________________________________________"<<endl;
    cout<<endl<<"Twój wybór: ";
	if(!(cin>>wybor))
	{
		cerr<<"Aby przejść dalej, należy wybrać numer z menu"<<endl;
	}
    cout<<endl;
}
void menu(int &wybor)
{
    cout<<"_________________________________________________"<<endl;
    cout<<"1. Dodaj element do Stosu"<<endl;
    cout<<"2. Usuń górny element Stosu"<<endl;
    cout<<"3. Usuń wszystkie elementy ze Stosu"<<endl;
    cout<<"0. Powrót"<<endl;
    cout<<"__________________________________________________"<<endl;
    cout<<endl<<"Twój wybór: ";
	if(!(cin>>wybor))
	{
		cerr<<"Aby przejść dalej, należy wybrać numer z menu"<<endl;
	}
    cout<<endl;
}
void tablicy()
{
    int wybor=1;
    while(wybor!=0)
    {
        system("clear");
        kotlet.wyswietl();

        menu(wybor);

        switch(wybor)
        {
            case 0:
            break;
            case 1:
            int x;
            cout<<"Podaj element, jaki chcesz dodać do stosu: ";
            cin>>x;
            kotlet.push(x);
            break;
            case 2:
            kotlet.pop();
            cin.get();
            cin.get();
            break;
            case 3:
            kotlet.del();
            cin.get();
            cin.get();
            break;
            case 4:
            cout<<kotlet.rozmiarek()<<endl;
            cin.get();
            cin.get();
            default:
            cout<<"Proszę wybrać od 1 do 3 lub wybrać 0, aby wyjść z programu"<<endl;
            cin.get();
            cin.get();
            break;
        }
    }
}
void bibliotece()
{
    int wybor=1;
    while(wybor!=0)
    {
        system("clear");
        if(stos.empty())
            cout<<"Stos jest pusty"<<endl;
        else
            wyswietl(stos);

        menu(wybor);

        switch(wybor)
        {
            case 0:
            break;
            case 1:
            int x;
            cout<<"Podaj element który chcesz dodać do stosu: ";
            cin>>x;
            stos.push(x);
            break;
            case 2:
            cout<<"Nastąpi usuniecie elementu ze stosu"<<endl;
            cin.get();
            cin.get();
            stos.pop();
            break;
            case 3:
            cout<<"Następi usuniecie wszystkich elementów ze Stosu"<<endl;;
            cin.get();
            cin.get();
            while(!stos.empty())
                stos.pop();
            break;
            default:
            cout<<"Proszę wybrać od 1 do 3 lub wybrać 0, aby wyjść z programu"<<endl;
            cin.get();
            cin.get();
            break;
        }
    }
}
void test(int wybor)
{
    system("clear");
    int ile;
    cout<<"Ile losowych elementów dodać do stosów? ";
    cin>>ile;

    srand(time(NULL));
    clock_t start = clock();
    for(int i=0; i<ile; i++)
    {
        int x=rand()%10;
        stos.push(x);
        //lista.dodaj(x);
    }
    clock_t stop = clock();
    cout<<"Czas dodania "<<ile<<" elementów do stosu STL wynosi: "<<(double)(stop-start) / CLOCKS_PER_SEC * 1000<<" ms"<<endl;

    start=clock();
    for(int i=0; i<ile; i++)
    {
        int x=rand()%10;
        kotlet.push(x);
    }
    stop = clock();
    cout<<"Czas dodania "<<ile<<" elementów do stosu na tablicy wynosi: "<<(double)(stop-start) / CLOCKS_PER_SEC * 1000<<" ms"<<endl;

    cin.get();
    cin.get();
}
int main()
{
    int wybor=1;
    while(wybor!=0)
    {
        system("clear");
        startowe(wybor);

        switch(wybor)
        {
            case 0:
            break;
            case 1:
            tablicy();
            break;
            case 2:
            bibliotece();
            break;
            case 3:
            test(wybor);
            break;
            default:
            cout<<"Proszę wybrać od 1 do 3 lub wybrać 0, aby wyjść z programu"<<endl;
            cin.get();
            cin.get();
            break;
        }
    }
    return 0;

}
