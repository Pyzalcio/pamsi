#ifndef STOS_H
#define STOS_H
#include <stack>
#include "Stos_tab.h"

template <typename typ>
ostream& operator<<(ostream& x,const stack<typ>& stos)
{
    stack<typ> pomoc=stos;
    typ t[stos.size()];
    int i=0;
    int dlugosc=pomoc.size();
    while(!pomoc.empty())
    {
        t[dlugosc-1-i]=pomoc.top();
        pomoc.pop();
        i++;
    }
    for(int i=dlugosc-1;i>=0;i--)
        x<<t[i]<<endl;
    return x;
}
template <typename typ>
void wyswietl(stack<typ> stos)
{
    cout<<stos<<endl;
}

#endif
