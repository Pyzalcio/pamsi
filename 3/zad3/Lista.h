#ifndef LISTA_H
#define LISTA_H

#include <iostream>
#include "element.h"

using namespace std;

template <typename T>
class Lista
{
    private:
    element<T> *poczatek=NULL;

    public:

    void dodaj(T x);
    void wyswietl();
    void usun(T x);
    void usun_all();
};

template <typename T>
void Lista<T>::dodaj(T x)
{
    element<T> *nowy = new element<T>;
    nowy->liczba=x;
    if(!poczatek)
    {
        poczatek=nowy;
    }
    else
    {
        element<T> *pomoc=poczatek;
        while(pomoc->nastepny)
        {
            pomoc=pomoc->nastepny;
        }
        pomoc->nastepny=nowy;
        nowy->nastepny=NULL;
    }
}
template <typename T>
void Lista<T>::wyswietl()
{
    cout<<"Zawartość listy:"<<endl;
    if(poczatek==NULL)
        cout<<"Lista jest pusta";
    else
    {
        element<T> *pomoc=poczatek;
        while(pomoc)
        {
            cout<<pomoc->liczba<<"  ";
            pomoc=pomoc->nastepny;
        }
    }
    cout<<endl;
}
template <typename T>
void Lista<T>::usun(T x)
{
    element<T> *pomoc=poczatek;
    element<T> *pomoc2=NULL;
    if(x==1)
    {
        pomoc=poczatek->nastepny;
        delete poczatek;
        poczatek=pomoc;
    }
    else
    {
        int i=1;
        while(pomoc && i<x)
        {
            if(i==x-1)
            {
                if(pomoc->nastepny==NULL)
                    pomoc2=NULL;

                else
                    pomoc2=pomoc->nastepny->nastepny;

                delete pomoc->nastepny;
                pomoc->nastepny=pomoc2;
            }
            pomoc=pomoc->nastepny;
            i++;
        }
    }
}
template <typename T>
void Lista<T>::usun_all()
{
    element<T> *pomoc=poczatek;
    while(poczatek)
    {
        pomoc=poczatek->nastepny;
        delete poczatek;
        poczatek=pomoc;
    }
}


#endif
