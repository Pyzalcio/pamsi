#include <iostream>
#include <cstdlib>
#include <stack>
#include "Stos_tab.h"
#include "Stos.h"
#include "time.h"

using namespace std;

int main()
{
    stack<int> pierwszy;
    stack<int> drugi;
    stack<int> trzeci;
    int ile;
    cout<<"Podaj liczbę krążków: ";
    cin>>ile;
    cout<<endl;
    for(int i=0; i<ile; i++)
        pierwszy.push(ile-i);

    wyswietl(pierwszy, drugi, trzeci);
    cout<<"Naciśnij przycisk, aby rozpocząć Hanoi"<<endl;
    cin.get();
    cin.get();
    clock_t start = clock();
    hanoi(ile,pierwszy, drugi, trzeci);
    wyswietl(pierwszy,drugi,trzeci);
    clock_t stop = clock();
    cout<<"Czas wykonania się funkcji Hanoi: "<<(double)(stop-start) / CLOCKS_PER_SEC * 1000<<" ms"<<endl;

    return 0;
}
