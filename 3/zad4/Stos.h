#ifndef STOS_H
#define STOS_H
#include <stack>
#include "Stos_tab.h"

template <typename typ>
ostream& operator<<(ostream& x,const stack<typ>& stos)
{
    stack<typ> pomoc=stos;
    int t[stos.size()];
    int i=0;
    int dlugosc=pomoc.size();
    while(!pomoc.empty())
    {
        t[dlugosc-1-i]=pomoc.top();
        pomoc.pop();
        i++;
    }
    if(stos.empty())
    {
            cout<<"Pusto"<<endl;
    }
    else
    {
        for(int i=dlugosc-1;i>=0;i--)
            x<<t[i]<<endl;
    }
    return x;
}

template <typename typ>
void wyswietl(stack<typ> pierwszy, stack<typ> drugi, stack<typ> trzeci)
{
    cout<<endl<<"-----"<<endl;
    cout<<pierwszy;
    cout<<"-----"<<endl;
    cout<<drugi;
    cout<<"-----"<<endl;
    cout<<trzeci;
    cout<<"-----"<<endl;
    cout<<endl<<"-------------------------------------------------------------------"<<endl;
}

template <typename typ>
void hanoi(int n, stack<typ> &A, stack<typ> &B, stack<typ> &C)
{
  if (n > 0)
  {
    wyswietl(A,B,C);
    hanoi(n-1, A, C, B);
    C.push(A.top());
    A.pop();
    hanoi(n-1, B, A, C);
  }
}

#endif
