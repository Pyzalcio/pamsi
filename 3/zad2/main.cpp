#include <iostream>
#include <cstdlib>
#include <queue>
#include "Kolejka_tab.h"
#include "Kolejka.h"

using namespace std;

Kolejka_tab<char> kotlet;
queue<char> kolejka;

void start(int &wybor)
{
    cout<<"__________________________________________________"<<endl;
    cout<<"1. Kolejka bazujący na tablicy"<<endl;
    cout<<"2. Kolejka w oparciu o biblioteke STL"<<endl;
    cout<<"0. Wyjście z programu"<<endl;
    cout<<"__________________________________________________"<<endl;
    cout<<endl<<"Twój wybór: ";
	if(!(cin>>wybor))
	{
		cerr<<"Aby przejść dalej, należy wybrać numer z menu"<<endl;
	}
    cout<<endl;
}
void menu(int &wybor)
{
    cout<<"_________________________________________________"<<endl;
    cout<<"1. Dodaj element do kolejki"<<endl;
    cout<<"2. Usuń pierwszy element kolejki"<<endl;
    cout<<"3. Usuń wszystkie elementy ze kolejki"<<endl;
    cout<<"0. Powrót"<<endl;
    cout<<"__________________________________________________"<<endl;
    cout<<endl<<"Twój wybór: ";
	if(!(cin>>wybor))
	{
		cerr<<"Aby przejść dalej, należy wybrać numer z menu"<<endl;
	}
    cout<<endl;
}
void tablicy()
{
    int wybor=1;
    while(wybor!=0)
    {
        system("clear");
        kotlet.wyswietl();

        menu(wybor);

        switch(wybor)
        {
            case 0:
            break;
            case 1:
            char x;
            cout<<"Podaj element, jaki chcesz dodać do kolejki: ";
            cin>>x;
            kotlet.push(x);
            break;
            case 2:
            kotlet.pop();
            cin.get();
            cin.get();
            break;
            case 3:
            kotlet.del();
            cin.get();
            cin.get();
            break;
            default:
            cout<<"Proszę wybrać od 1 do 3 lub wybrać 0, aby wyjść z programu"<<endl;
            cin.get();
            cin.get();
            break;
        }
    }
}
void bibliotece()
{
    int wybor=1;
    while(wybor!=0)
    {
        system("clear");
        if(kolejka.empty())
            cout<<"Kolejka jest pusta"<<endl;
        else
            wyswietl(kolejka);

        menu(wybor);

        switch(wybor)
        {
            case 0:
            break;
            case 1:
            char x;
            cout<<"Podaj element który chcesz dodać do kolejki: ";
            cin>>x;
            kolejka.push(x);
            break;
            case 2:
            cout<<"Nastąpi usuniecie elementu ze kolejki"<<endl;
            cin.get();
            cin.get();
            kolejka.pop();
            break;
            case 3:
            cout<<"Następi usuniecie wszystkich elementów ze kolejki"<<endl;
            cin.get();
            cin.get();
            while(!kolejka.empty())
                kolejka.pop();
            break;
            default:
            cout<<"Proszę wybrać od 1 do 3 lub wybrać 0, aby wyjść z programu"<<endl;
            cin.get();
            cin.get();
            break;
        }
    }
}
int main()
{
    int wybor=1;
    while(wybor!=0)
    {
        system("clear");

        start(wybor);

        switch(wybor)
        {
            case 0:
            break;
            case 1:
            tablicy();
            break;
            case 2:
            bibliotece();
            break;
            default:
            cout<<"Proszę wybrać od 1 do 2 lub wybrać 0, aby wyjść z programu"<<endl;
            cin.get();
            cin.get();
            break;
        }
    }
    return 0;
}
