#ifndef KOLEJKA_H
#define KOLEJKA_H
#include <queue>
#include "Kolejka_tab.h"

template <typename typ>
ostream& operator<<(ostream& x,const queue<typ>& kolejka)
{
    queue<typ> pomoc=kolejka;
    typ t[kolejka.size()];
    int i=0;
    int dlugosc=pomoc.size();
    while(!pomoc.empty())
    {
        t[dlugosc-1-i]=pomoc.front();
        pomoc.pop();
        i++;
    }
    for(int i=dlugosc-1; i>=0;i--)
        x<<t[i]<<" ";
    return x;
}
template <typename typ>
void wyswietl(queue<typ> kolejka)
{
    cout<<kolejka<<endl;
}

#endif
