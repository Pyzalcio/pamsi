#ifndef STOS_TAB_H
#define STOS_TAB_H
#include <iostream>

using namespace std;

template <typename typ>
class Kolejka_tab
{
    int rozmiar;
    typ *tab;

    public:
    Kolejka_tab();

    ~Kolejka_tab();

    void push(typ &x);
    void pop();
    void del();
    void wyswietl();
};

template <typename typ>
Kolejka_tab<typ>::Kolejka_tab()
{
    tab = new typ[rozmiar];
}
template <typename typ>
Kolejka_tab<typ>::~Kolejka_tab()
{
    delete [] tab;
}
template <typename typ>
void Kolejka_tab<typ>::push(typ &x)
{
    rozmiar++;
    if(rozmiar!=0)
    {
        typ z[rozmiar-1];
        for(int i=0; i<rozmiar-1; i++)
        {
            z[i]=tab[i];
        }
        for(int i=1; i<rozmiar; i++)
        {
            tab[i]=z[i-1];
        }
    }
    tab[0]=x;
}
template <typename typ>
void Kolejka_tab<typ>::pop()
{
    if(rozmiar==0)
        cout<<"Kolejka jest pusta!"<<endl;
    else
    {
        cout<<"Z kolejki zostanie usunięty element: "<<tab[rozmiar]<<endl;
        rozmiar--;
    }
}
template <typename typ>
void Kolejka_tab<typ>::del()
{
    if(rozmiar==0)
        cout<<"Kolejka jest pusta!"<<endl;
    else
    {
        cout<<"Wszystkie elementy z kolejki zostaną usunięte"<<endl;
            rozmiar=0;
    }
}
template <typename typ>
void Kolejka_tab<typ>::wyswietl()
{
    if(rozmiar==0)
    {
        cout<<"Kolejka jest pusta"<<endl;
    }
    else
    {
        for(int i=rozmiar-1; i>=0; i--)
            cout<<tab[i]<<"  ";
    }
    cout<<endl;
}
#endif
