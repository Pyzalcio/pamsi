#include <iostream>
#include <cstdlib>
#include <stack>
#include "Stos_tab.h"
#include "Stos.h"
#include "time.h"

using namespace std;

Stos_tab<char> kotlet;
stack<char> stos;

void start(int &wybor)
{
    cout<<"__________________________________________________"<<endl;
    cout<<"1. Stos bazujący na tablicy"<<endl;
    cout<<"2. Stos w oparciu o biblioteke STL"<<endl;
    cout<<"0. Wyjście z programu"<<endl;
    cout<<"__________________________________________________"<<endl;
    cout<<endl<<"Twój wybór: ";
	if(!(cin>>wybor))
	{
		cerr<<"Aby przejść dalej, należy wybrać numer z menu"<<endl;
	}
    cout<<endl;
}
void menu(int &wybor)
{
    cout<<"_________________________________________________"<<endl;
    cout<<"1. Dodaj element do Stosu"<<endl;
    cout<<"2. Usuń górny element Stosu"<<endl;
    cout<<"3. Usuń wszystkie elementy ze Stosu"<<endl;
    cout<<"0. Powrót"<<endl;
    cout<<"__________________________________________________"<<endl;
    cout<<endl<<"Twój wybór: ";
	if(!(cin>>wybor))
	{
		cerr<<"Aby przejść dalej, należy wybrać numer z menu"<<endl;
	}
    cout<<endl;
}
void tablicy()
{
    int wybor=1;
    while(wybor!=0)
    {
        system("clear");
        kotlet.wyswietl();

        menu(wybor);

        switch(wybor)
        {
            case 0:
            break;
            case 1:
            char x;
            cout<<"Podaj element, jaki chcesz dodać do stosu: ";
            cin>>x;
            kotlet.push(x);
            break;
            case 2:
            kotlet.pop();
            cin.get();
            cin.get();
            break;
            case 3:
            kotlet.del();
            cin.get();
            cin.get();
            break;
            default:
            cout<<"Proszę wybrać od 1 do 3 lub wybrać 0, aby wyjść z programu"<<endl;
            cin.get();
            cin.get();
            break;
        }
    }
}
void bibliotece()
{
    int wybor=1;
    while(wybor!=0)
    {
        system("clear");
        if(stos.empty())
            cout<<"Stos jest pusty"<<endl;
        else
            wyswietl(stos);

        menu(wybor);

        switch(wybor)
        {
            case 0:
            break;
            case 1:
            char x;
            cout<<"Podaj element który chcesz dodać do stosu: ";
            cin>>x;
            stos.push(x);
            break;
            case 2:
            cout<<"Nastąpi usuniecie elementu ze stosu"<<endl;
            cin.get();
            cin.get();
            stos.pop();
            break;
            case 3:
            cout<<"Następi usuniecie wszystkich elementów ze Stosu"<<endl;;
            cin.get();
            cin.get();
            while(!stos.empty())
                stos.pop();
            break;
            default:
            cout<<"Proszę wybrać od 1 do 3 lub wybrać 0, aby wyjść z programu"<<endl;
            cin.get();
            cin.get();
            break;
        }
    }
}
int main()
{
    int wybor=1;
    while(wybor!=0)
    {
        system("clear");
        start(wybor);

        switch(wybor)
        {
            case 0:
            break;
            case 1:
            tablicy();
            break;
            case 2:
            bibliotece();
            break;
            default:
            cout<<"Proszę wybrać od 1 do 2 lub wybrać 0, aby wyjść z programu"<<endl;
            cin.get();
            cin.get();
            break;
        }
    }
    return 0;
}
