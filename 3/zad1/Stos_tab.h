#ifndef STOS_TAB_H
#define STOS_TAB_H
#include <iostream>

using namespace std;

template <typename typ>
class Stos_tab
{
    int rozmiar;
    typ *tab;

    public:
    Stos_tab();

    ~Stos_tab();

    void push(typ &x);
    void pop();
    void del();
    void wyswietl();
};

template <typename typ>
Stos_tab<typ>::Stos_tab()
{
    tab = new typ[rozmiar];
}
template <typename typ>
Stos_tab<typ>::~Stos_tab()
{
    delete [] tab;
}
template <typename typ>
void Stos_tab<typ>::push(typ &x)
{
    rozmiar++;
    tab[rozmiar]=x;
}
template <typename typ>
void Stos_tab<typ>::pop()
{
    if(rozmiar==0)
        cout<<"Stos jest pusty!"<<endl;
    else
    {
        cout<<"Ze stosu zostanie usunięty element: "<<tab[rozmiar]<<endl;
        rozmiar--;
    }
}
template <typename typ>
void Stos_tab<typ>::del()
{
    if(rozmiar==0)
        cout<<"Stos jest pusty!"<<endl;
    else
    {
        cout<<"Wszystkie elementy ze stosu zostaną usunięte"<<endl;
            rozmiar=0;
    }
}
template <typename typ>
void Stos_tab<typ>::wyswietl()
{
    if(rozmiar==0)
    {
        cout<<"Stos jest pusty"<<endl;
    }
    else
    {
        for(int i=rozmiar; i>0; i--)
            cout<<tab[i]<<endl;
    }
}
#endif
