#include <iostream>
#include <set>
#include <ctime>

#include "Drzewo_AVL.h"

using namespace std;

int main()
{
    int a, b, c, d, e, f, g, h, i;
    cin>>a>>b>>c>>d>>e>>f>>g>>h>>i;
    Drzewo_AVL<int> tree;
    tree.wstaw(a);//3
    cout<<"Wstawiam: "<<a<<endl;
    sleep(1);
    tree.wstaw(b,tree.get_korzen());//8
    cout<<"Wstawiam: "<<b<<endl;
    sleep(1);
    tree.wstaw(c,tree.get_korzen());//7
    cout<<"Wstawiam: "<<c<<endl;
    sleep(1);
    //rotuje po dodaniu 7

    tree.wstaw(d,tree.get_korzen());//12
    cout<<"Wstawiam: "<<d<<endl;
    sleep(1);
    tree.wstaw(e,tree.get_korzen());//4
    cout<<"Wstawiam: "<<e<<endl;
    sleep(1);
    tree.wstaw(f,tree.get_korzen());//11
    cout<<"Wstawiam: "<<f<<endl;
    sleep(1);
    tree.wstaw(g,tree.get_korzen());//13
    cout<<"Wstawiam: "<<g<<endl;
    sleep(1);
    tree.wstaw(h,tree.get_korzen());//14
    cout<<"Wstawiam: "<<h<<endl;
    sleep(1);
    tree.wstaw(i,tree.get_korzen());//15
    cout<<"Wstawiam: "<<i<<endl;
    sleep(1);

    cout<<endl<<"Przejście in-post drzewa: ";
    tree.wyswietl_przej_in(tree.get_korzen());
    cout<<endl<<"Przejście in-post drzewa po usunięciu elementu ("<<i<<"): ";
    tree.usun(i);
    tree.wyswietl_przej_in(tree.get_korzen());
    cout<<endl<<endl;
    cout<<"Korzen: "<<tree.get_korzen()->get_lisc()<<endl;
    cout<<"Lewy syn: "<<tree.get_korzen()->get_lewe()->get_lisc()<<endl;
    cout<<"Prawy syn: "<<tree.get_korzen()->get_prawe()->get_lisc()<<endl;

    cout<<"Wysokość drzewa: "<<tree.wysokosc()<<endl;

    return 0;
}
