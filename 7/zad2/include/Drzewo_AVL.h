#ifndef DRZEWO_AVL_H
#define DRZEWO_AVL_H

#include "Binarne.h"
#include "Galaz.h"

template <typename Object>
class Drzewo_AVL : public Binarne<Object>
{
    void rotuj_lewo(Galaz<Object>* pomoc);
    void rotuj_prawo(Galaz<Object>* pomoc);
    void rotuj_lewo_prawo(Galaz<Object>* pomoc);
    void rotuj_prawo_lewo(Galaz<Object>* pomoc);
    Galaz<Object>* zmien_balane_i_rotacje(Galaz<Object>* pomoc);
    void nowe_balance(Galaz<Object>* pomoc,const Object& wartosc);
    void rotuj(Galaz<Object>* pomoc);
    void zmien_balance(Galaz<Object>* pomoc2);
    Galaz<Object>* znajdz(const Object& wartosc);

public:

    void wstaw(const Object& wartosc);
    void wstaw(const Object& wartosc, Galaz<Object> * pomoc);
    void usun(const Object& wartosc);
};

template <typename Object>
void Drzewo_AVL<Object>::wstaw(const Object& wartosc)
{
    Binarne<Object>::wstaw(wartosc);
    Binarne<Object>::get_korzen()->set_balance(0);
}

template <typename Object>
void Drzewo_AVL<Object>::wstaw(const Object& wartosc, Galaz<Object>* pomoc)
{
    Galaz<Object> * pomoc2 = Binarne<Object>::wstaw(wartosc, pomoc);
    if(pomoc2->get_lewe())
    {
        if (pomoc2->get_lewe()->get_lisc()==wartosc)
        {
            pomoc2->get_lewe()->set_balance(0);
            pomoc2->set_balance(pomoc2->get_balance() + 1);

            if(!pomoc2->get_prawe() && pomoc2->get_korzen())
                nowe_balance(pomoc2->get_korzen(), pomoc2->get_lisc());
        }
    }
    if(pomoc2->get_prawe())
    {
        if(pomoc2->get_prawe()->get_lisc()==wartosc)
        {
            pomoc2->get_prawe()->set_balance(0);
            pomoc2->set_balance(pomoc2->get_balance() - 1);

            if (pomoc2->get_lewe() == nullptr && pomoc2->get_korzen() != nullptr)
                nowe_balance(pomoc2->get_korzen(), pomoc2->get_lisc());
        }
    }
}

template <typename Object>
void Drzewo_AVL<Object>::usun(const Object& wartosc)
{
    Binarne<Object>::usun(znajdz(wartosc));
    zmien_balance(Drzewo_AVL<Object>::get_korzen());
    while (this->get_korzen()->get_balance()>1 || this->get_korzen()->get_balance()<-1)
    {
        zmien_balance(Drzewo_AVL<Object>::get_korzen());

        Galaz<Object>* pomoc2=zmien_balane_i_rotacje(Drzewo_AVL<Object>::get_korzen());

        if (pomoc2)
            rotuj(pomoc2);
    }
}

template <typename Object>
Galaz<Object>* Drzewo_AVL<Object>::zmien_balane_i_rotacje(Galaz<Object>* pomoc)
{
    if (pomoc)
    {
        if (pomoc->get_lewe())
            zmien_balane_i_rotacje(pomoc->get_lewe());

        if (pomoc->get_prawe())
            zmien_balane_i_rotacje(pomoc->get_prawe());


        if (pomoc->get_balance()>1 || pomoc->get_balance() <-1)
        {
            return pomoc;
        }
    }
}

template <typename Object>
void Drzewo_AVL<Object>::rotuj(Galaz<Object>* pomoc)
{
    if(pomoc->get_balance()>1)
    {
        if(pomoc->get_lewe()->get_lewe())
            rotuj_lewo(pomoc->get_lewe());

        else
            rotuj_prawo_lewo(pomoc->get_lewe()->get_prawe());
    }
    else if(pomoc->get_balance()<-1)
    {
        if(pomoc->get_prawe()->get_prawe())
            rotuj_prawo(pomoc->get_prawe());

        else
            rotuj_lewo_prawo(pomoc->get_prawe()->get_lewe());
    }
}

template <typename Object>
void Drzewo_AVL<Object>::zmien_balance(Galaz<Object>* pomoc)
{
    if(pomoc)
    {
        int wysokosc_l = 0;
        int wysokosc_p = 0;

        if (pomoc->get_lewe())
            wysokosc_l=Drzewo_AVL<Object>::wysokosc(pomoc->get_lewe());

        if (pomoc->get_prawe())
            wysokosc_p=Drzewo_AVL<Object>::wysokosc(pomoc->get_prawe());

        pomoc->set_balance(wysokosc_l-wysokosc_p);

        if (pomoc->get_lewe())
            zmien_balance(pomoc->get_lewe());

        if (pomoc->get_prawe())
            zmien_balance(pomoc->get_prawe());
    }
}

template <typename Object>
void Drzewo_AVL<Object>::rotuj_lewo(Galaz<Object> * pomoc)
{
    Galaz<Object> * pomoc2 = pomoc->get_korzen();
    if(!pomoc->get_korzen()->get_korzen())
    {
        Binarne<Object>::root = this->get_korzen()->get_lewe();
        pomoc->set_korzen(nullptr);
    }
    else
    {
        if(pomoc2->get_korzen()->get_lewe()==pomoc2)
            pomoc2->get_korzen()->set_lewe(pomoc);

        else
            pomoc2->get_korzen()->set_prawe(pomoc);

        pomoc->set_korzen(pomoc2->get_korzen());
    }
    if(pomoc->get_prawe())
    {
        pomoc2->set_lewe(pomoc->get_prawe());
        pomoc->get_prawe()->set_korzen(pomoc2);
    }
    else
        pomoc2->set_lewe(nullptr);

    pomoc->set_prawe(pomoc2);
    pomoc2->set_korzen(pomoc);

    if(pomoc->get_prawe())
    {
        pomoc->set_balance(0);
        pomoc2->set_balance(0);
    }
    else
    {
        pomoc2->set_balance(0);
        pomoc->set_balance(-1);
    }
    cout<<endl<<"ROTACJA W LEWO:"<<endl;
    cout<<endl<<"Element: "<<pomoc2->get_korzen()->get_lisc()<<" wskakuje na miejsce: "<<pomoc->get_prawe()->get_lisc()<<", a jego prawa galaz zostaje lewym synem jego prawego syna."<<endl;
}

template <typename Object>
void Drzewo_AVL<Object>::rotuj_prawo(Galaz<Object>* pomoc)
{
    Galaz<Object> * pomoc2 = pomoc->get_korzen();
    if(!pomoc->get_korzen()->get_korzen())
    {
        Binarne<Object>::root=this->get_korzen()->get_prawe();
        pomoc->set_korzen(nullptr);
    }
    else
    {
        if(pomoc2->get_korzen()->get_prawe()==pomoc2)
            pomoc2->get_korzen()->set_prawe(pomoc);

        else
            pomoc2->get_korzen()->set_lewe(pomoc);

        pomoc->set_korzen(pomoc2->get_korzen());
    }
    if(pomoc->get_lewe())
    {
        pomoc2->set_prawe(pomoc->get_lewe());
        pomoc->get_lewe()->set_korzen(pomoc2);
    }
    else
        pomoc2->set_prawe(nullptr);

    pomoc->set_lewe(pomoc2);
    pomoc2->set_korzen(pomoc);

    if(pomoc->get_lewe())
    {
        pomoc->set_balance(0);
        pomoc2->set_balance(0);
    }
    else
    {
        pomoc2->set_balance(0);
        pomoc->set_balance(-1);
    }
    cout<<endl<<"ROTACJA W PRAWO:";
    cout<<endl<<"Element: "<<pomoc2->get_korzen()->get_lisc()<<" wskakuje na miejsce: "<<pomoc->get_lewe()->get_lisc()<<", a jego lewa galaz zostaje prawym synem jego lewego syna."<<endl;
}

template <typename Object> void Drzewo_AVL<Object>::rotuj_lewo_prawo(Galaz<Object> * pomoc)
{
    rotuj_lewo(pomoc);
    rotuj_prawo(pomoc);
}

template <typename Object>
void Drzewo_AVL<Object>::rotuj_prawo_lewo(Galaz<Object> * pomoc)
{
    rotuj_prawo(pomoc);
    rotuj_lewo(pomoc);
}

template <typename Object>
Galaz<Object>* Drzewo_AVL<Object>::znajdz(const Object &wartosc)
{
    Object wartosc_pomoc2 = wartosc + 1;
    Galaz<Object> * pomoc2 = this->get_korzen();
    while (wartosc_pomoc2 != wartosc)
    {
        while (pomoc2->get_lisc() > wartosc)
            pomoc2 = pomoc2->get_lewe();

        while (pomoc2->get_lisc() < wartosc)
            pomoc2 = pomoc2->get_prawe();

        wartosc_pomoc2 = pomoc2->get_lisc();
    }

    return pomoc2;
}

template <typename Object>
void Drzewo_AVL<Object>::nowe_balance(Galaz<Object> * pomoc, const Object &wartosc)
{
    if (pomoc->get_lewe() != nullptr)
    {
        if (pomoc->get_lewe()->get_lisc() == wartosc)
        {
            pomoc->set_balance(pomoc->get_balance() + 1);

            if (pomoc->get_balance() >= 2 || pomoc->get_balance()<-1)
                this->rotuj(pomoc);

            if (pomoc->get_korzen() != nullptr && pomoc->get_balance() != 0)
                nowe_balance(pomoc->get_korzen(), pomoc->get_lisc());
        }
    }
    if(pomoc->get_prawe())
    {
        if (pomoc->get_prawe()->get_lisc() == wartosc)
        {
            pomoc->set_balance(pomoc->get_balance() - 1);

            if (pomoc->get_balance()>1 || pomoc->get_balance()<-1)							//
                this->rotuj(pomoc);														//

            if (pomoc->get_korzen() && pomoc->get_balance() != 0)
                nowe_balance(pomoc->get_korzen(), pomoc->get_lisc());
        }
    }
}

#endif
