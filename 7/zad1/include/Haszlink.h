#ifndef HASZ_H
#define HASZ_H

#include <iostream>
#include "Lista.h"

using namespace std;

template <typename Object>
class Haszlink
{
    Lista<int> *Tab;
    int rozmiar;

public:
    Haszlink(int x);
    ~Haszlink();

    void dodaj(int x);
    void dodaj(int *Tab, int rozmiar);
    void usun(int x);
    int szukaj(int x);
    void wyswietl();
};

template <typename Object>
Haszlink<Object>::Haszlink(int x)
{
    rozmiar=x;
    Tab=nullptr;
    if(rozmiar>0)
    {
    	 Tab=new Lista<int> [rozmiar];
    }
}

template <typename Object>
Haszlink<Object>::~Haszlink()
{
    if(Tab)
        delete[] Tab;
}

template <typename Object>
void Haszlink<Object>::dodaj(int x)
{
    int k=x%rozmiar;
    //cout<<"i="<<i<<"  ";
    //cout<<"x="<<x<<endl;
    Tab[k].wstawOstatni(x);
}

template <typename Object>
void Haszlink<Object>::dodaj(int *Tab, int rozmiar)
{
    for(int i=0; i<rozmiar; i++)
    {
        dodaj(Tab[i]);
    }
}

template <typename Object>
void Haszlink<Object>::usun(int x)
{
    int k=x%rozmiar;
	int pomoc;

	if((pomoc=Tab[k].Znajdz_Index(x))!=-1)
        Tab[k].usun_all(pomoc);
}

template <typename Object>
void Haszlink<Object>::wyswietl()
{
    for(int i=0; i<rozmiar; i++)
    {
        cout<<i<<": ";
        Tab[i].wyswietl();
        cout<<endl;
    }
}

template <typename Object>
int Haszlink<Object>::szukaj(int x)
{
    int k=x%rozmiar;
    //cout<<i<<endl;
    if(Tab[k].Znajdz_Index(x)==-1)
        return Tab[k].Znajdz_Index(x);
    else
        return k;
}

#endif
