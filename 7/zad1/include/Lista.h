#ifndef LISTA_H
#define LISTA_H

#include "Element.h"
#include <iostream>
#include <string>

using namespace std;

template <typename Object>
class Lista
{
private:
    Element<Object> *poczatek=nullptr;
    Element<Object> *koniec=nullptr;

public:

    int rozmiar() const;
    bool isEmpty() const;
    Object& pierwszy();
    Object& ostatni();
    void wstawPierwszy(const Object& x);
    void usunPierwszy() ;
    void wstawOstatni(const Object& x);
    void usunOstatni();
    void wyswietl();
    int Znajdz_Index(Object Data);
    void usun_all(int i);
    Object* operator [] (int indx) const;
};

template<typename Object>
int Lista<Object>::Znajdz_Index(Object Data)
{
    Element<Object> *pomoc = poczatek;
    int k = 0;
    while(pomoc)
    {
        if(pomoc->wartosc==Data)
            return k;

        pomoc = pomoc->nastepny;
        ++k;
    }
    return -1;
}

template<typename Object>
Object * Lista<Object>::operator[](int indx) const
{
	if(indx<0)
        return nullptr;

	Element<Object> *pomoc = poczatek;
	while(pomoc && indx)
	{
		pomoc = pomoc->get_next();
		--indx;
	}
	if(pomoc) return &pomoc->wartosc;
	return nullptr;
}

template<typename Object>
void Lista<Object>::usun_all(int i)
{
    while(poczatek)
    {
        if( isEmpty() )
        {
            cout<<"Lista jest pusta"<<endl;
        }
        else
        {
            Element<Object> *pomoc=poczatek;
            if(pomoc->nastepny)
            {
                pomoc=pomoc->nastepny;
            }
            else
                pomoc=NULL;

            free(poczatek);
            poczatek=pomoc;
        }
    }
}

template <typename Object>
void Lista<Object>::wyswietl()
{
    //cout<<"Zawartosc listy:"<<endl;
    if(!poczatek)
        cout<<"Pusto";
    else
    {
        Element<Object> *pomoc=poczatek;
        while(pomoc)
        {
            cout<<pomoc->wartosc<<"  ";
            pomoc=pomoc->nastepny;
        }
    }
}

template <typename Object>
int Lista<Object>::rozmiar() const
{
    Element<Object> *pomoc=poczatek;
    int i=0;
    while(pomoc)
    {
        i++;
        pomoc=pomoc->nastepny;
    }
    return i;
}

template <typename Object>
bool Lista<Object>::isEmpty() const
{
    Element<Object> *pomoc=poczatek;
    if(pomoc)
        return false;
    else
        return true;
}

template <typename Object>
Object& Lista<Object>::pierwszy()
{
    return poczatek->wartosc;
}

template <typename Object>
Object& Lista<Object>::ostatni()
{
    return koniec->wartosc;
}

template <typename Object>
void Lista<Object>::wstawPierwszy(const Object& x)
{
    Element<Object> *nowy = new Element<Object>;
    nowy->wartosc=x;
    if (poczatek)
    {
        Element<Object> *pomoc = poczatek;

        poczatek=nowy;
        poczatek->nastepny=pomoc;
    }
    else
    {
        poczatek=nowy;
        koniec=nowy;
    }
}

template <typename Object>
void Lista<Object>::usunPierwszy()
{
    if( isEmpty() )
    {
        cout<<"Lista jest pusta"<<endl;
    }
    else
    {
        Element<Object> *pomoc=poczatek;
        cout<<"Pierwszy element listy zostanie usunięty"<<endl;
        if(pomoc->nastepny)
        {
            pomoc=pomoc->nastepny;
        }
        else
            pomoc=nullptr;

        free(poczatek);
        poczatek=pomoc;
    }
}
template <typename Object>
void Lista<Object>::wstawOstatni(const Object& x)
{
    Element<Object> *nowy = new Element<Object>;
    nowy->wartosc=x;

    if (poczatek)
    {

        Element<Object> *pomoc=poczatek;

        while (pomoc->nastepny)
        {
            pomoc=pomoc->nastepny;
        }

        pomoc->nastepny=nowy;
        nowy->nastepny=nullptr;
        koniec=nowy;
    }
    else
        wstawPierwszy(x);
}
template <typename Object>
void Lista<Object>::usunOstatni()
{
    if(isEmpty())
    {
        cout<<"Lista jest pusta"<<endl;
    }
    else
    {
        Element<Object> *pomoc=koniec;
        cout<<"Ostatni element listy zostanie usunięty"<<endl;
        if(pomoc->poprzedni)
        {
            pomoc=pomoc->poprzedni;
        }
        else
            pomoc=nullptr;

        free(koniec);
        koniec=pomoc;

    }
}

#endif
