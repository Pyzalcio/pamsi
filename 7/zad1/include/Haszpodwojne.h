#ifndef HASZPODWOJNE_H
#define HASZPODWOJNE_H

#include <iostream>

#include "Lacznik.h"

using namespace std;

template<typename Object>
class Haszpodwojne
{

    Lacznik<Object> *Tab;
    int rozmiar;
    int q;
    int l_probek;
public:

    Haszpodwojne(int x, int Qwe);
    ~Haszpodwojne();
    bool jest_L_pierwsza(int x);
    void nadajQ();
    void dodaj(int x);
    int szukaj(int x);
    void usun(int x);
    void dodaj(int *Tab, int rozmiar);
    void wyswietl();
};


template<typename Object>
Haszpodwojne<Object>::Haszpodwojne(int x, int Qwe)
{
    rozmiar=x;
    Tab=nullptr;
    q=Qwe;
    l_probek=0;
    if(rozmiar>0 && jest_L_pierwsza(rozmiar))
    {
        Tab = new Lacznik<Object>[rozmiar];
        if(q==0)
            nadajQ();
    }
}
template<typename Object>
Haszpodwojne<Object>::~Haszpodwojne()
{
    if(Tab) delete[] Tab;
}

template<typename Object>
bool Haszpodwojne<Object>::jest_L_pierwsza(int x)
{
    if(x<2)
        return false;

    for(int i=2; i*i<=x; i++)
    {
        if(x%i==0)
            return false;
    }
    return true;
}
template<typename Object>
void Haszpodwojne<Object>::nadajQ()
{
    //q = 2;
    for(int i=rozmiar-1; i>=0; i--)
    {
        if(jest_L_pierwsza(i))
        {
            q = i;
            return;
        }
    }
}

template<typename Object>
void Haszpodwojne<Object>::dodaj(int x)
{
    int h = x%rozmiar;
    int d = q-(x%q);
    int k;
    for(int j=0; j<rozmiar; j++)
    {
        k = (h+j*d)%rozmiar;
        l_probek++;
        if(Tab[k].Empty)
        {
            Tab[k].wartosc = x;
            Tab[k].Empty = false;
            return;
        }
    }
}

template<typename Object>
void Haszpodwojne<Object>::usun(int x)
{
    int h = x%rozmiar;
    int d = q-(x%q);
    int k;
    for(int i=0; i<rozmiar; i++)
    {
        k = (h+i*d)%rozmiar;
        if(!Tab[k].Empty && Tab[k].wartosc==x)
        {
            Tab[k].Empty = true;
            return;
        }
    }
}

template<typename Object>
void Haszpodwojne<Object>::dodaj(int *Tab, int rozmiar)
{
    for(int i=0; i<rozmiar; i++)
    {
        dodaj(Tab[i]);
    }
}

template<typename Object>
void Haszpodwojne<Object>::wyswietl()
{
    for(int i=0; i<rozmiar; i++)
    {
        if(Tab[i].Empty)
            cout<<i<<": pusto"<<endl;

        else
            cout<<i<<": "<<Tab[i].wartosc<<endl;
    }
    cout<<"Całkowita ilość próbek: "<<l_probek<<endl;
}

template<typename Object>
int Haszpodwojne<Object>::szukaj(int x)
{
    int h = x%rozmiar;
    int d = q-(x%q);
    int k;
    for(int i=0; i<rozmiar; i++)
    {
        k = (h+i*d)%rozmiar;
        if(!Tab[k].Empty && Tab[k].wartosc==x)
        {
            return k;
        }
    }
    return -1;
}

#endif
