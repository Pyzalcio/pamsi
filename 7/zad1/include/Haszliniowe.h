#ifndef HASZLINIOWE_H
#define HASZLINIOWE_H

#include <iostream>

#include "Lacznik.h"

using namespace std;

template<typename Object>
class Haszliniowe
{
    Lacznik<Object> *Tab;
    int rozmiar;
public:
    Haszliniowe(int x);
    ~Haszliniowe();

    void dodaj(int x);
    void dodaj(int *Tab, int rozmiar);
    void usun(int x);
    int szukaj(int x);
    void wyswietl();
};

template<typename Object>
Haszliniowe<Object>::Haszliniowe(int x)
{
    rozmiar=x;
    Tab=nullptr;
    if(rozmiar>0) Tab = new Lacznik<Object>[rozmiar];
}

template<typename Object>
Haszliniowe<Object>::~Haszliniowe()
{
    if(Tab) delete[] Tab;
}

template<typename Object>
void Haszliniowe<Object>::dodaj(int x)
{
    int k=x%rozmiar;
    int Temp;
    for(int i=0; i<rozmiar; i++)
    {
        Temp = (k+i)%rozmiar;
        if(Tab[Temp].Empty)
        {
            Tab[Temp].wartosc = x;
            Tab[Temp].Empty = false;
            return;
        }
    }
}

template<typename Object>
void Haszliniowe<Object>::usun(int x)
{
    int k = x%rozmiar;
    int Temp;
    for(int i=0; i<rozmiar; i++)
    {
        Temp = (k+i)%rozmiar;
        if(!Tab[Temp].Empty && Tab[Temp].wartosc==x)
        {
            Tab[Temp].Empty=true;
            return;
        }
    }
}

template<typename Object>
void Haszliniowe<Object>::dodaj(int *Tab, int rozmiar)
{
    for(int i=0; i<rozmiar; i++)
    {
        dodaj(Tab[i]);
    }
}

template<typename Object>
void Haszliniowe<Object>::wyswietl()
{
    for(int i=0; i<rozmiar; i++)
    {
        if(Tab[i].Empty)
            cout<<i<<": ---"<<endl;

        else
            cout<<i<<": "<<Tab[i].wartosc<<endl;
    }
}

template<typename Object>
int Haszliniowe<Object>::szukaj(int x)
{
    int k = x%rozmiar;
    int Temp;
    for(int i=0; i<rozmiar; i++)
    {
        Temp = (k+i)%rozmiar;
        if(!Tab[Temp].Empty && Tab[Temp].wartosc==x) return Temp;
    }
    return -1;
}

#endif
