#include <iostream>
#include <unistd.h>

#include "include/Haszlink.h"
#include "include/Haszliniowe.h"
#include "include/Haszpodwojne.h"


bool czy_powtorka(int liczba, int *tab, int g)
{
    for(int i=0; i<g; i++)
    {
        if(tab[i]==liczba)
            return true;
    }
    return false;
}
void poka(int *tab, int ile)
{
    cout<<"Tablica: ";
    for(int i=0; i<ile; i++)
    {
        cout<<tab[i]<<"  ";
    }
    cout<<endl<<endl;
}
void losuj(int *tab, int ile, int x)
{
    int liczba;
    int g;
    srand(time(NULL));
    for(int i=0; i<ile; i++)
    {
        tab[i]=0;
    }

    for(int i=0; i<ile; i++)
    {
        do
        {
            liczba=rand()%x;
            //cout<<liczba<<endl;
            //sleep(1);
        }while(czy_powtorka(liczba, tab, g));

        tab[i]=liczba;
        g++;
    }
    poka(tab, ile);
}
int main()
{
    int x;
    int ile=8;
    int zakres=100;
    int *tab;
    cout<<"Ilość wprowadzanych elementów: ";
    cin>>ile;
    system("clear");

    //Linkowanie
    cout<<"Linkowanie: "<<endl;
    tab = new int [ile];
    losuj(tab, ile, zakres);
    Haszlink<int> Tab1(11);
    Tab1.dodaj(tab, 8);
    Tab1.wyswietl();

    cout<<endl<<"Usuń z tablicy: ";
    cin>>x;
    if(Tab1.szukaj(x)==-1)
        cout<<"W tablicy nie ma takiej wartości!"<<endl;
    else
    {
    Tab1.usun(x);
    Tab1.wyswietl();
    }
    cout<<endl<<"Znajdź w tablicy: ";
    cin>>x;
    if(Tab1.szukaj(x)==-1)
    {
        cout<<"Brak wartości w tablicy"<<endl;
    }
    else
        cout<<Tab1.szukaj(x)<<endl;

    cin.get();
    cin.get();
    system("clear");
    delete [] tab;


    //Liniowe
    cout<<"Próbkowanie liniowe: "<<endl;
    tab = new int [ile];
    losuj(tab,ile, zakres);
    Haszliniowe<int> Tab2(11);
    Tab2.dodaj(tab, 8);
    Tab2.wyswietl();

    cout<<endl<<"Usuń z tablicy: ";
    cin>>x;
    if(Tab2.szukaj(x)==-1)
        cout<<"W tablicy nie ma takiej wartości!"<<endl;
    else
    {
    Tab2.usun(x);
    Tab2.wyswietl();
    }
    cout<<endl<<"Znajdź w tablicy: ";
    cin>>x;
    if(Tab2.szukaj(x)==-1)
    {
        cout<<"Brak wartości w tablicy"<<endl;
    }
    else
        cout<<Tab2.szukaj(x)<<endl;

    cin.get();
    cin.get();
    system("clear");
    delete [] tab;



    //Podwójne
    cout<<"Haszowanie podwójne: "<<endl;
    tab = new int [ile];
    losuj(tab,ile, zakres);
    Haszpodwojne<int> Tab3(13,7);
    Tab3.dodaj(tab, 8);
    Tab3.wyswietl();

    cout<<endl<<"Usuń z tablicy: ";
    cin>>x;
    if(Tab3.szukaj(x)==-1)
        cout<<"W tablicy nie ma takiej wartości!"<<endl;
    else
    {
    Tab3.usun(x);
    Tab3.wyswietl();
    }
    cout<<endl<<"Znajdź w tablicy: ";
    cin>>x;
    if(Tab3.szukaj(x)==-1)
    {
        cout<<"Brak wartości w tablicy"<<endl;
    }
    else
        cout<<Tab3.szukaj(x)<<endl;

    delete [] tab;

    return 0;
}
