#include <stdio.h>
#include <iostream>
#include <time.h>
#include <cstdlib>
#include <unistd.h>
#include <fstream>

using namespace std;

void menu(int &wybor)
{
	cout<<"1. Wypełnienie tablicy wartościami losowymi"<<endl;
	cout<<"2. Wyznaczenie najwyższej wartości tablicy"<<endl;
	cout<<"3. Wyświetlenie tablicy"<<endl;
	cout<<"4. Wyjście z programu"<<endl;

	if(!(cin>>wybor))
	{
		cerr<<"Aby przejść dalej, należy wybrać numer z menu"<<endl;
	}
}
void alokacja(int &a, int &b)
{
	cout<<"Podaj pierwszy wymiar tablicy: ";
	cin>>a;
	cout<<"Podaj drugi wymiar tablicy: ";
	cin>>b;
}
void kasuj(int &a, int** tab)
{
	for (int i = 0; i<a; i++)
	{
		delete [] tab[i];
	}
}
void wypelnij(int &a, int &b, int** tab)
{

	srand(time(NULL));
	for(int i=0;i<a;i++)
	{
		for(int j=0;j<b;j++)
		{
			tab[i][j]=rand()%99+1;
		}
	}
	cout<<"Wypełniono"<<endl;
}
void max(int &a, int &b, int** tab)
{
	int x=tab[0][0];
	for(int i=0;i<a;i++)
	{
		for(int j=0;j<b;j++)
		{
			if(tab[i][j]>x)
			{
				x=tab[i][j];
			}
		}
	}
	cout<<"Największa wartość występująca w tablicy: "<<x<<endl;
}
void wyswietl(int &a, int &b, int **tab)
{
	for(int i=0;i<a;i++)
	{
		cout<<endl;
		for(int j=0;j<b;j++)
		{
			cout<<tab[i][j]<<"	";
		}
		cout<<endl;
	}
	cout<<endl;
}

int main()
{
	int a,b;
	int wybor;
	int **tab;
	system("clear");
	cout<<"Program przedstawiający tablicę dynamiczną"<<endl;

    alokacja(a,b);
    tab = new int* [a];
    for(int i=0;i<a;i++)
    {
        tab[i] = new int[b];
    }
	while(wybor!=4)
	{
		system("clear");
		menu(wybor);
		switch(wybor)
		{
			case 1:
			system("clear");
			wypelnij(a,b,tab);
			cin.get();
            cin.get();
            break;
			case 2:
			system("clear");
			max(a,b,tab);
			cin.get();
            cin.get();
			break;
			case 3:
			system("clear");
			wyswietl(a,b,tab);
			cin.get();
            cin.get();
			break;

		}
	}

	kasuj(a, tab);

	return 0;
}
