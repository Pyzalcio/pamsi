#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include <string>

using namespace std;

/*bool jestPal(string testStr)
{
     int dlugosc=testStr.length();
     string odwrocone;
     size_t size = testStr.size();
     odwrocone = jestPal(testStr.substr(,dlugosc-2));
     cout<<odwrocone<<endl;

     if(odwrocone==testStr)
     {
        return true;
     }
     else
     {
        return false;
     }
}*/
bool jestPal(string testStr)
{
	int dlugosc=testStr.length();

	if( (testStr.length()==1) || (testStr.length()==0))
	{
	return true;
	}

	if( testStr[0] == testStr[dlugosc - 1])
	{
		return jestPal( testStr.substr( 1, dlugosc-2) );
    }

	else
	{
        return false;
    }
}
int main()
{
	string testStr;

    cout<<"Proszę wpisać palindrom, aby sprawdzić jego poprawność: ";
    cin>>testStr;

    if (jestPal(testStr))
    {
        cout<<"Wyraz jest palindromem"<<endl;
    }
    else
    {
        cout<<"Wyraz nie jest palindromem"<<endl;
    }

	return 0;
}
