#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <fstream>
#include <time.h>

using namespace std;



void menu(int &wybor)
{
	cout<<"1. Wczytaj tablice z pliku tekstowego"<<endl;
	cout<<"2. Wczytaj tablice z pliku binarnego"<<endl;
	cout<<"3. Wyświetlenie tablicy"<<endl;
	cout<<"4. Zapisz tablice do pliku tekstowego"<<endl;
	cout<<"5. Zapisz tablice do pliku binarnego"<<endl;
	//cout<<"6. Wypełnij tablice losowo"<<endl;
	cout<<"0. Wyjscie z programu"<<endl;

	if(!(cin>>wybor))
	{
		cerr<<"Aby przejść dalej, należy wybrać numer z menu"<<endl;
	}
}
void wypelnij(int x, int *tab)
{
	srand(time(NULL));
	for(int i=0;i<x;i++)
	{
        tab[x]=rand()%99+1;
	}
	cout<<"Wypełniono"<<endl;
}
void wyswietl(int x, int *tab)
{
	for(int i=0;i<x;i++)
	{
        cout<<tab[i]<<" ";
	}
}

void wczytaj(int &x, int *tab)
{
    x=0;
    fstream plik;
    plik.open("plik.txt", ios::in);

    //string nazwa;

    if(plik.good()==true)
    {
        //plik>>nazwa;
        /*for(int i=0;i<x;i++)
        {
            plik<<tab[i]<<;
        }*/

        while(!plik.eof())
        {
            int a;
            plik>>a;
            //cout<<a<<"  ";
            x++;
        }
        x--;

        //cout<<endl<<x<<endl;
        cout<<endl<<"Tablica została wczytana poprawnie!"<<endl;
    }

    else
    {
        cerr<<"Nie udało się wczytać tablicy z pliku!"<<endl;
    }

    plik.close();
    plik.open("plik.txt", ios::in);

    delete [] tab;

    tab = new int [x];

    for(int i=0; i<x; i++)
    {
        plik>>tab[i];
    }

    //cout<<nazwa<<endl;

    plik.close();
}
void wczytaj_bi(int &x, int *tab)
{
    x=0;
    fstream plik;
    plik.open("plik.txt", ios::in | ios::binary);

    if(plik.good()==true)
    {
        while(!plik.eof())
        {
            int a;
            plik>>a;
            //cout<<a<<"  ";
            x++;
        }
        x--;

        //cout<<endl<<x<<endl;
        cout<<endl<<"Tablica została wczytana poprawnie!"<<endl;
    }


    else
    {
        cerr<<"Nie udało się wczytać tablicy z pliku!"<<endl;
    }

    plik.close();
    plik.open("plik.bin", ios::in);

    delete [] tab;

    tab = new int [x];

    for(int i=0; i<x; i++)
    {
        plik>>tab[i];
    }

    plik.close();
}
void zapisz (int x, int *tab)
{
    fstream plik;
    plik.open("pliczek.txt", ios::out);

    if(plik.good()==true)
    {

        for(int i=0;i<x;i++)
        {
            plik<<tab[i]<<" ";
        }
        cout<<"Tablica została zapisana poprawnie!"<<endl;
    }
    else
    {
        cerr<<"Nie udało się zapisać tablicy do pliku!"<<endl;
    }

    plik.close();
}
void zapisz_bi (int x, int *tab)
{
    fstream plik;
    plik.open("pliczek.bin", ios::out | ios::binary);

    if(plik.good()==true)
    {

        for(int i=0;i<x;i++)
        {
            plik<<tab[i]<<" ";
        }
        cout<<"Tablica została zapisana poprawnie!"<<endl;
    }
    else
    {
        cerr<<"Nie udało się zapisać tablicy do pliku!"<<endl;
    }

    plik.close();
}
int main()
{
	int x=5;
	int wybor=1;
	int *tab;
	cout<<"Program przedstawiający jednowymiarową tablicę dynamiczną"<<endl;



    tab = new int [x];

	while(wybor!=0)
	{
        system("clear");
		menu(wybor);
		switch(wybor)
		{
			case 1:
			system("clear");
			wczytaj(x,tab);
			cin.get();
			cin.get();
			break;
			case 2:
            system("clear");
			wczytaj_bi(x,tab);
			cin.get();
			cin.get();
			break;
			case 3:
			system("clear");
            wyswietl(x,tab);
            cin.get();
            cin.get();
			break;
			case 4:
			system("clear");
			zapisz(x,tab);
			cin.get();
			cin.get();
			break;
			case 5:
			system("clear");
			zapisz_bi(x,tab);
			cin.get();
			cin.get();
			break;

		}
	}

	delete [] tab;

	return 0;
}
