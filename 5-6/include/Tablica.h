#ifndef SCALANIE_H
#define SCALANIE_H
#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <ctime>
#include <unistd.h>

#define ROZMIAR 1000000
#include "Intro.h"

using namespace std;


template <typename typ>
class Tablica
{
    typ *tab;
    typ tab2[ROZMIAR];
    int rozmiar=0;
    public:
    Tablica();
    ~Tablica();
    void wypelnij(int &ile);
    void wyswietl();
    void del();
    void szybkie(int q, int p);
    void scalanie(int q, int p);
    void scal(int pocz, int srodek, int kon);
    void odwroc(int ile);
    void wroc();

    void intro(int ile);
};

template <typename typ>
void Tablica<typ>::odwroc(int ile)
{
    typ pomoc[ROZMIAR];
    for(int i=0; i<ile; i++)
    {
        pomoc[ile-i-1]=tab[i];
    }
    for(int i=0; i<ile; i++)
    {
        tab[i]=pomoc[i];
    }
}
template <typename typ>
void Tablica<typ>::wroc()
{
    for(int i=0; i<rozmiar; i++)
    {
        tab[i]=tab2[i];
    }
}
template <typename typ>
void Tablica<typ>::intro(int ile)
{
    introsort(tab, ile, 0);
}

template <typename typ>
Tablica<typ>::Tablica()
{
    tab = new typ [ROZMIAR];
    //t = new typ [ROZMIAR];
}
template <typename typ>
Tablica<typ>::~Tablica()
{
    delete tab;
    //delete t;
}
template <typename typ>
void Tablica<typ>::wyswietl()
{
    cout<<"Zawartość tablicy: "<<endl;
    if(rozmiar==0)
    {
        cout<<"Tablica jest pusta"<<endl;
    }
    else
    {
        for(int i=0; i<rozmiar; i++)
            cout<<tab[i]<<"  ";
    }
    cout<<endl;
}
template <typename typ>
void Tablica<typ>::del()
{
    if(rozmiar==0)
        cout<<"Kolejka jest pusta!"<<endl;
    else
    {
        //cout<<"Wszystkie elementy z kolejki zostaną usunięte"<<endl;
            rozmiar=0;
            delete tab;
            tab = new typ [ROZMIAR];
    }
}
template <typename typ>
void Tablica<typ>::wypelnij(int &ile)
{
    int x;
    srand(time(NULL));
    int r=rozmiar;
    for(int i=r; i<r+ile; i++)
    {
        x=rand()%100;
        tab[i]=x;
        rozmiar++;
    }
    ile=ile+r;
    for(int i=0; i<ile; i++)
    {
        tab2[i]=tab[i];
    }
    //cout<<"Tablica została wypełniona"<<endl;
}
template <typename typ>
void Tablica<typ>::szybkie(int q, int p)
{
    typ os=tab[(q+p)/2];
    int i,j,x;
    i=q;
    j=p;
    do
    {
        while(tab[i]<os) i++;
        while(tab[j]>os) j--;

        if(i<=j)
        {
            x=tab[i];
            tab[i]=tab[j];
            tab[j]=x;
            i++;
            j--;
        }
    }while(i<=j);

    if(j>q) szybkie(q, j);
    if(i<p) szybkie(i, p);
}
template <typename typ>
void Tablica<typ>::scal(int q, int srodek, int p)
{
    typ pomoc[ROZMIAR];
    int i,j,x;
    for(i=q; i<=p; i++)
        pomoc[i]=tab[i];

    i=q;
    j=srodek+1;
    x=q;
    while(i<=srodek && j<=p)
    {
        if(pomoc[i]<pomoc[j])
            tab[x++]=pomoc[i++];
        else
            tab[x++]=pomoc[j++];
    }
    while(i<=srodek)
        tab[x++]=pomoc[i++];

}
template <typename typ>
void Tablica<typ>::scalanie(int q, int p)
{
    int srodek;
    if (q<p)
    {
        srodek=(q+p)/2;
        scalanie(q, srodek);
        scalanie(srodek+1, p);
        scal(q, srodek, p);
    }
}
#endif
