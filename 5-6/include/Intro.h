#ifndef INTRO_H
#define INTRO_H

#include <iostream>

using namespace std;
/*
template <typename typ>
void reheapify(typ *tab, int x, int n);

template <typename typ>
void heapsort(typ *tab, int n);

template <typename typ>
void insertionsort(typ *tab, int n);

template <typename typ>
int partycjowanie(typ *tab, int n);

template <typename typ>
void introsort(typ *tab, int n, int x);

*/
#include <iostream>

#include <algorithm>

using namespace std;

template <typename typ>
void reheapify(typ *tab, int x, int n)
{
    int left = 2*x+1;
    int right = 2*x+2;
    int candidate = left;
    while ((left < n) && (tab[x] < tab[candidate]))
    {
        left=2*x+1;
        right=2*x+2;
        candidate=left;
        if (right<n && tab[right] > tab[left])
            candidate = right;
        swap(tab[x], tab[candidate]);
        x = candidate;
    }
}

template <typename typ>
void heapsort(typ *tab, int n)
{
    for (int i = (n-2)/2; i >= 0; --i)
    {
        reheapify(tab, i, n);
    }
    for (int i = n-1; i > 0; --i)
    {
        swap(tab[0], tab[i]);
        reheapify(tab, 0, i);
    }
}

template <typename typ>
void insertionsort(typ *tab, int n)
{
    for (int i = 1; i < n; ++i)
    {
        for (int j = i; j > 0 && tab[j-1] > tab[j]; --j)
        {
            swap(tab[j],tab[j-1]);
        }
    }
}

template <typename typ>
int partycjowanie(typ *tab, int n)
{
    typ pivot = tab[n/2];
    int i = -1;
    int j = n;
    while (i<j) {
        while (tab[++i] < pivot);
        while (tab[--j] > pivot);
        swap(tab[i], tab[j]);
    }
    return j;
}

template <typename typ>
void introsort(typ *tab, int n, int x)
{
    if (n < 2) return;
    if (n < 22)
    {
        insertionsort(tab, n);
        return;
    }
    if (x > (int)floor(2*log(n)/M_LN2))
    {
        heapsort(tab, n);
        return;
    }
    int p = partycjowanie(tab, n);
    introsort(tab, p + 1, x + 1);
    introsort(&tab[p + 1], n - p - 1, x + 1);
}

#endif
