#include <fstream>
#include <ctime>

#include "Tablica.h"

using namespace std;

//void test()
int main()
{
    Tablica<int> tab;
    fstream plik;
    plik.open("dane.txt", ios::out);
    clock_t start1, start2, start3;
    clock_t stop1, stop2, stop3;
    int ile=10000;
    tab.wypelnij(ile);

    plik<<"Wszystkie elementy losowe dla 10 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl;
    tab.del();
    cout<<"Wszystkie elementy losowe dla 10 000 elementów"<<endl;

    ile=50000;
    tab.wypelnij(ile);
    plik<<"Wszystkie elementy losowe dla 50 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl;
    tab.del();
    cout<<"Wszystkie elementy losowe dla 50 000 elementów"<<endl;

    ile=100000;
    tab.wypelnij(ile);
    plik<<"Wszystkie elementy losowe dla 100 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"Wszystkie elementy losowe dla 100 000 elementów"<<endl;

    ile=500000;
    tab.wypelnij(ile);
    plik<<"Wszystkie elementy losowe dla 500 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"Wszystkie elementy losowe dla 500 000 elementów"<<endl;

    ile=1000000;
    tab.wypelnij(ile);
    plik<<"Wszystkie elementy losowe dla 1 000 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"Wszystkie elementy losowe dla 1 000 000 elementów"<<endl;

    ile=10000;
    tab.wypelnij(ile);
    plik<<"Odwrocone elementy dla 10 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        tab.odwroc(ile);
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        tab.odwroc(ile);
        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        tab.odwroc(ile);
        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"Odwrocone elementy losowe dla 10 000 elementów"<<endl;

    ile=50000;
    tab.wypelnij(ile);
    plik<<"Odwrocone elementy dla 50 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        tab.odwroc(ile);
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        tab.odwroc(ile);
        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        tab.odwroc(ile);
        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"Odwrocone elementy losowe dla 50 000 elementów"<<endl;

    ile=100000;
    tab.wypelnij(ile);
    plik<<"Odwrocone elementy dla 100 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        tab.odwroc(ile);
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        tab.odwroc(ile);
        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        tab.odwroc(ile);
        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"Odwrocone elementy losowe dla 100 000 elementów"<<endl;

    ile=500000;
    tab.wypelnij(ile);
    plik<<"Odwrocone elementy dla 500 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        tab.odwroc(ile);
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        tab.odwroc(ile);
        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        tab.odwroc(ile);
        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"Odwrocone elementy losowe dla 500 000 elementów"<<endl;

    ile=1000000;
    tab.wypelnij(ile);
    plik<<"Odwrocone elementy dla 1 000 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        tab.odwroc(ile);
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        tab.odwroc(ile);
        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        tab.odwroc(ile);
        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"Odwrocone elementy losowe dla 1 000 000 elementów"<<endl;

    ile=2500;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=7500;
    tab.wypelnij(ile);
    plik<<"25% posortowanych dla 10 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"25% posortowanych dla 10 000 elementów"<<endl;

    ile=5000;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=5000;
    tab.wypelnij(ile);
    plik<<"50% posortowanych dla 10 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"50% posortowanych dla 10 000 elementów"<<endl;

    ile=7500;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=2500;
    tab.wypelnij(ile);
    plik<<"75% posortowanych dla 10 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"75% posortowanych dla 10 000 elementów"<<endl;

    ile=9500;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=500;
    tab.wypelnij(ile);
    plik<<"95% posortowanych dla 10 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"95% posortowanych dla 10 000 elementów"<<endl;

    ile=9900;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=100;
    tab.wypelnij(ile);
    plik<<"99% posortowanych dla 10 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"99% posortowanych dla 10 000 elementów"<<endl;

    ile=9970;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=30;
    tab.wypelnij(ile);
    plik<<"99,7% posortowanych dla 10 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"99,7% posortowanych dla 10 000 elementów"<<endl;


    ile=12500;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=37500;
    tab.wypelnij(ile);
    plik<<"25% posortowanych dla 50 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"25% posortowanych dla 50 000 elementów"<<endl;

    ile=25000;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=25000;
    tab.wypelnij(ile);
    plik<<"50% posortowanych dla 50 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"50% posortowanych dla 50 000 elementów"<<endl;

    ile=37500;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=12500;
    tab.wypelnij(ile);
    plik<<"75% posortowanych dla 50 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"75% posortowanych dla 50 000 elementów"<<endl;

    ile=47500;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=2500;
    tab.wypelnij(ile);
    plik<<"95% posortowanych dla 50 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"95% posortowanych dla 50 000 elementów"<<endl;

    ile=49500;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=500;
    tab.wypelnij(ile);
    plik<<"99% posortowanych dla 50 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"99% posortowanych dla 50 000 elementów"<<endl;

    ile=49850;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=150;
    tab.wypelnij(ile);
    plik<<"99,7% posortowanych dla 50 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"99,7% posortowanych dla 50 000 elementów"<<endl;


    ile=25000;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=75000;
    tab.wypelnij(ile);
    plik<<"25% posortowanych dla 100 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"25% posortowanych dla 100 000 elementów"<<endl;

    ile=50000;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=50000;
    tab.wypelnij(ile);
    plik<<"50% posortowanych dla 100 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"50% posortowanych dla 100 000 elementów"<<endl;

    ile=75000;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=25000;
    tab.wypelnij(ile);
    plik<<"75% posortowanych dla 100 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"75% posortowanych dla 100 000 elementów"<<endl;

    ile=95000;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=5000;
    tab.wypelnij(ile);
    plik<<"95% posortowanych dla 100 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"95% posortowanych dla 100 000 elementów"<<endl;

    ile=99000;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=1000;
    tab.wypelnij(ile);
    plik<<"99% posortowanych dla 100 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"99% posortowanych dla 100 000 elementów"<<endl;

    ile=99700;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=300;
    tab.wypelnij(ile);
    plik<<"99,7% posortowanych dla 100 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"99,7% posortowanych dla 100 000 elementów"<<endl;

    ile=125000;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=375000;
    tab.wypelnij(ile);
    plik<<"25% posortowanych dla 500 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"25% posortowanych dla 500 000 elementów"<<endl;

    ile=250000;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=250000;
    tab.wypelnij(ile);
    plik<<"50% posortowanych dla 500 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"50% posortowanych dla 500 000 elementów"<<endl;

    ile=375000;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=125000;
    tab.wypelnij(ile);
    plik<<"75% posortowanych dla 500 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"75% posortowanych dla 500 000 elementów"<<endl;

    ile=475000;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=25000;
    tab.wypelnij(ile);
    plik<<"95% posortowanych dla 500 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"95% posortowanych dla 500 000 elementów"<<endl;

    ile=495000;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=5000;
    tab.wypelnij(ile);
    plik<<"99% posortowanych dla 500 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"99% posortowanych dla 500 000 elementów"<<endl;

    ile=498500;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=1500;
    tab.wypelnij(ile);
    plik<<"99,7% posortowanych dla 500 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"99,7% posortowanych dla 500 000 elementów"<<endl;

    ile=250000;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=750000;
    tab.wypelnij(ile);
    plik<<"25% posortowanych dla 1 000 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"25% posortowanych dla 1 000 000 elementów"<<endl;

    ile=500000;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=500000;
    tab.wypelnij(ile);
    plik<<"50% posortowanych dla 1 000 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"50% posortowanych dla 1 000 000 elementów"<<endl;

    ile=750000;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=250000;
    tab.wypelnij(ile);
    plik<<"75% posortowanych dla 1 000 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"75% posortowanych dla 1 000 000 elementów"<<endl;

    ile=950000;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=50000;
    tab.wypelnij(ile);
    plik<<"95% posortowanych dla 1 000 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"95% posortowanych dla 1 000 000 elementów"<<endl;

    ile=990000;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=10000;
    tab.wypelnij(ile);
    plik<<"99% posortowanych dla 1 000 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"99% posortowanych dla 1 000 000 elementów"<<endl;

    ile=997000;
    tab.wypelnij(ile);
    tab.intro(ile);
    ile=3000;
    tab.wypelnij(ile);
    plik<<"99,7% posortowanych dla 1 000 000 elementów (quick, scal, intro):"<<endl;
    for(int i=0; i<100; i++)
    {
        start1=clock();
        tab.szybkie(0, ile-1);
        stop1=clock();
        tab.wroc();

        start2=clock();
        tab.scalanie(0, ile-1);
        stop2=clock();
        tab.wroc();

        start3=clock();
        tab.intro(ile);
        stop3=clock();
        tab.wroc();
        plik<<stop1-start1<<"   "<<stop2-start2<<"  "<<stop3-start3<<endl;
    }
    plik<<endl; tab.del();
    cout<<"99,7% posortowanych dla 1 000 000 elementów"<<endl;

    cout<<"Test zakończony. Dane zostały zapisane do pliku 'dane.txt'."<<endl;

    return 0;
}




/*
void menu(int &wybor)
{
    cout<<"------------------------------"<<endl;
    cout<<"1. Wypełnij tablice losowymi wartościami"<<endl;
    cout<<"2. Usuń tablice"<<endl;
    cout<<"3. Sortowanie szybkie"<<endl;
    cout<<"4. Sortowanie przez scalanie"<<endl;
    cout<<"5. Sortowanie introspektywne"<<endl;
    cout<<"6. Test do sprawozdania"<<endl;
    cout<<"0. Wyjście z programu"<<endl;
    cout<<"------------------------------"<<endl;
    cout<<"Twój wybór: ";
    if(!(cin>>wybor))
	{
		cerr<<"Aby przejść dalej, należy wybrać numer z menu"<<endl;
	}
}
int main()
{
    Tablica<int> tab;
    int wybor=1;
    while(wybor!=0)
    {
        system("clear");
        tab.wyswietl();
        menu(wybor);
        switch(wybor)
        {
        case 1:
            int ile;
            cout<<"Iloma losowymi wartościami chcesz wypełnić tablice?  ";
            cin>>ile;
            tab.wypelnij(ile);
            cin.get();
            cin.get();
            break;
        case 2:
            tab.del();
            cin.get();
            cin.get();
            break;
        case 3:
            cout<<"Zostanie wykonane sortowanie szybkie"<<endl;
            cin.get();
            cin.get();
            tab.szybkie(0, ile-1);
            break;
        case 4:
            cout<<"Zostanie wykonane sortowanie przez scalanie"<<endl;
            cin.get();
            cin.get();
            tab.scalanie(0, ile-1);
            break;
        case 5:
            cout<<"Zostanie wykonane sortowanie introspektywne"<<endl;
            cin.get();
            cin.get();
            tab.intro(ile);
            break;
        case 6:
            system("clear");
            test();
            cin.get();
            cin.get();
            break;
        case 7:
            tab.wroc();
            break;
        case 0:
            break;
        default:
            cout<<"Proszę wybrać jedną opcje z menu (od 0 do 5) !"<<endl;
            break;
        }
    }

    return 0;
}*/
